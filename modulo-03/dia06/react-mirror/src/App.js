import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios'

class App extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    let ep = this.listaEpisodios.episodioAleatorio
    this.state = {
      episodio: ep,
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( {
      episodio,
    } )
  }

  marcarComoAssistido() {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio,
    } )
  }

  registrarNota( evt ) {
    const { episodio } = this.state
    episodio.avaliar( evt.target.value )
    this.setState( {
      episodio
    } )
    let registra = true
    this.mensagemNota( registra )
  }

  mensagemNota( registra ) {
    if( registra ) {
      return (
        <div>
          <div>
            <div>Nota registrada com sucesso!</div>
          </div>
        </div>
      )
    } else {
      return <div>Nada</div>
    }
  }

  geraCampoDeNota() {
     // https://reactjs.org/docs/conditional-rendering.html
     return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para este episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) } value={ this.state.episodio.nota}></input>
            </div>
          )
        }
      </div>
    )
  }

  render() {
  //const episodio = this.listaEpisodios.episodioAleatorio

    const { episodio } = this.state

    return (
      <div className="App">
        <header className="App-header">
        <h2>{ episodio.nome }</h2>
        <img src={ episodio.thumbUrl } alt={ episodio.nome}></img>
        <span>Já assisti? { episodio.assistido ? 'sim' : 'não' }, { episodio.qtdVezesAssistido } ve(zes)</span>
        <span>{ episodio.duracaoEmMin }</span>
        <span>{ episodio.temporadaEpisodio }</span>
        <span>{ episodio.assistido ? 'Sim': 'Não'}</span>
        <span>{ episodio.nota || 'sem nota'}</span>
        { this.geraCampoDeNota() }
        { this.mensagemNota() }
        <button onClick={ this.sortear.bind( this ) }>Próximo</button>
        <button onClick={ this.marcarComoAssistido.bind( this ) }>Já assisti</button>
        
        </header>
      </div>
    );
  }
}

//<button onClick={ this.toggleAssistido.bind( this ) }>{ (episodio.assistido ) ? 'Marcar como \'Não Assistido\'' : 'Marcar como \'Assistido\''}</button>

export default App;
