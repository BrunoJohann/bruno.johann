const data = new Date()
const anoAtual = data.getFullYear()
var dataGlobal = anoAtual

function anoReferencia ( data ) {
    dataGlobal = data
}

function validacaoAno ( series ) { 
    if( dataGlobal === anoAtual ){
        return series.anoEstreia > anoAtual 
    } else {
        return series.anoEstreia > dataGlobal
    }
}

series.invalidas = 
    function () {
        var resultado = series.filter(validacaoAno)
        return resultado.map( e => e.titulo)
}

series.filtrarPorAno = 
    function ( ano ) {
        anoReferencia( ano )
        var resultado = series.filter(validacaoAno)
        dataGlobal = anoAtual
        return resultado.map( e => e.titulo)
}

series.procurarPorNome = 
    function ( nome ) {
        var resultado = series.filter( nome )
        return resultado.map( e => e.elenco)
}