import React, { Component } from 'react'

import ListaEpisodios from '../models/listaEpisodios'
import EpisodiosApi from '../models/episodiosApi'

import EpisodioUi from '../components/episodioUi'

import MensagemFlash from '../components/shared/mensagemFlash'
import MeuInputNumero from '../components/shared/meuInputNumero'
import MeuBotao from '../components/shared/meuBotao'

import Mensagens from '../constants/mensagens'
import './paginaInicial.css'

export default class PaginaInicial extends Component {
  constructor( props ) {
    super( props )
    this.episodiosApi = new EpisodiosApi()
    // definindo estado inicial de um componente
    this.state = {
      //episodio: this.listaEpisodios.episodioAleatorio,
      deveExibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  componentDidMount() {
    // esse setTimeout é para simular lentidão de 3 segs
    this.episodiosApi.buscar()
      .then( episodiosDoServidor => {
        this.listaEpisodios = new ListaEpisodios( episodiosDoServidor.data )
        setTimeout( () => {
          this.setState( { episodio: this.listaEpisodios.episodioAleatorio } )
        }, 1500 )
      } )
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( {
      episodio, deveExibirMensagem: false
    } )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio,
    } )
  }

  exibeMensagem = ( { cor, mensagem } ) => {
    this.setState( {
      cor, mensagem, deveExibirMensagem: true
    } )
  } 

  registrarNota = ( { valor, erro } ) => {
    this.setState( {
      deveExibirErro: erro
    } )
    if ( erro ) {
      // se tiver erro no campo, atualizamos estado com exibição e já retornamos,
      // para não rodar o código como se estivesse válida a obrigatoriedade
      return;
    }
    
    const { episodio } = this.state
    let cor, mensagem
    if ( episodio.validarNota( valor ) ) {
      episodio.avaliar( valor ).then( () => {
        cor = 'verde'
        mensagem = Mensagens.SUCESSO.REGISTRO_NOTA
        this.exibeMensagem( { cor, mensagem } )
      } )
    } else {
      cor = 'vermelho'
      mensagem = Mensagens.ERRO.NOTA_INVALIDA
      this.exibeMensagem( { cor, mensagem } )
    }
  }

  atualizarMensagem = devoExibir => {
    this.setState( {
      deveExibirMensagem: devoExibir
    } )
  }

  render() {
    const { episodio, deveExibirMensagem, mensagem, cor, deveExibirErro } = this.state
    const { listaEpisodios } = this
    return (
      !episodio ? (
        <h3>Aguarde...</h3>
      ) : (
        <React.Fragment>
          <MensagemFlash atualizarMensagem={ this.atualizarMensagem } cor={ cor } deveExibirMensagem={ deveExibirMensagem } mensagem={ mensagem } segundos={ 5 } />
          <EpisodioUi episodio={ episodio } />
          <MeuInputNumero placeholder="1 a 5"
            mensagemCampo="Qual sua nota para este episódio?"
            obrigatorio={ true }
            atualizarValor={ this.registrarNota }
            deveExibirErro={ deveExibirErro }
            visivel={ episodio.assistido || false }/>
          <div className="botoes">
            <MeuBotao cor="verde" quandoClicar={ this.sortear } texto="Próximo" />
            <MeuBotao cor="azul" quandoClicar={ this.marcarComoAssistido } texto="Já assisti"/> 
            <MeuBotao cor="vermelho" link="/avaliacoes" dadosNavegacao={ { listaEpisodios } } texto="Ver notas!"/>
            <MeuBotao cor="azul" link="/listaEpisodios" dadosNavegacao={ { listaEpisodios } } texto="Lista de episodios"/>
          </div>
        </React.Fragment>
      )
    )
  }
}
