import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import MeuBotao from '../components/shared/meuBotao'

// function ordemDefinitiva( a, b ) {
//   const resultadoTemporario = a.temporada - b.temporada
//   return resultadoTemporario === 0 ? a.ordemEpisodio - b.ordemEpisodio : resultadoTemporario
// } 

// // function ordenarPorData( a, b ) {
// //   return 
// // }

// function ordenarPorDuracao( a, b ) {
//   // const { listaEpisodios } = this.props.location.state
//   // const temporadaParaOrdena = listaEpisodios.todos.sort( ( a, b ) => a.duracao - b.duracao )
//   return a.duracao - b.duracao
// }

// const TodosEpisodios = props => {
//   const { listaEpisodios } = props.location.state
//   const temporadaParaOrdena = listaEpisodios.todos.sort( ordemDefinitiva )
//   return (
//     <React.Fragment>
//       { temporadaParaOrdena.map( e => <Link className="link" to={{ pathname: "/episodio", state: { e } }} value={ e.id }><li key={ e.id }>{ `${ e.nome } - ${ e.temporada } - ${ e.ordemEpisodio }` }</li></Link> ) }
//       <div className="botoes">
//         <MeuBotao cor="verde" quandoClicar={ listaEpisodios.ordenarPorDuracao  } texto="Ordenar por duração" />
//         <MeuBotao cor="azul" texto="botão 2" />
//       </div>  
//     </React.Fragment>
//     )
// }

// export default TodosEpisodios

export default class TodosEpisodios extends Component {

  constructor( props ) {
    super( props )
    this.listaEpisodios = this.props.location.state.listaEpisodios
    this.state = {    
      funcaoOrdenacao: () => { },
      sentidoOrdenacao: 'asc',
      listaOrdenada: this.listaEpisodios.todos
    }
  }

  tipoOrdemTemporada = () => {
    const { sentidoOrdenacao } = this.state
    this.setState( {
      sentidoOrdenacao: sentidoOrdenacao === 'asc' ? 'desc' : 'asc',
      funcaoOrdenacao: ( a, b ) => {
        const resultadoTemporario = sentidoOrdenacao === 'asc' ? a.temporada - b.temporada : b.temporada - a.temporada
        return resultadoTemporario === 0 ? 
          ( sentidoOrdenacao === 'asc' ? a.ordemEpisodio - b.ordemEpisodio : b.ordemEpisodio - a.ordemEpisodio ) :
            resultadoTemporario
      }
    } )
  }

  tipoOrdemDuracao = () => {
    const { sentidoOrdenacao } = this.state
    this.setState( {
      funcaoOrdenacao: ( a, b ) => sentidoOrdenacao === 'asc' ? a.duracao - b.duracao : b.duracao - a.duracao,
      sentidoOrdenacao: sentidoOrdenacao === 'asc' ? 'desc' : 'asc'
    } )
  }

  render() {  
    
    return (
      <React.Fragment>
       { this.state.listaOrdenada.sort( this.state.funcaoOrdenacao ).map( e => <Link key={ e.id } className="link" to={{ pathname: "/episodio", state: { e } }} value={ e.id }><li>{ `${ e.nome } - ${ e.temporada } - ${ e.ordemEpisodio } - Duração:${ e.duracao }` }</li></Link> ) }
       <div className="botoes">
         <MeuBotao cor="verde" quandoClicar={ this.tipoOrdemDuracao } texto="Ordenar por duração" />
         <MeuBotao cor="azul" quandoClicar={ this.tipoOrdemTemporada } texto="botão 2" />
       </div>  
     </React.Fragment>
    )
  }
}