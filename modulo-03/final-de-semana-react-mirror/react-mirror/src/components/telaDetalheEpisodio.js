import React, { Component } from 'react'
import EpisodiosApi from '../models/episodiosApi';

// function promesaDetalhes(id) {
//     const episodioApi = new EpisodiosApi()
//     return episodioApi.buscarDetalhes( id ).then( episodiosDoServidor => {
//         return new ListaEpisodios( episodiosDoServidor.data )
//     } )
// }



// const TelaDetalheEpisodio  = props => {
//     const { e } = props.location.state
//     // const listaEpisodioDetalhada = this.promesaDetalhes( e.id)
//     const episodioApi = new EpisodiosApi()
//     const novaLista = episodioApi.buscarDetalhes( e.id ).then( listaDetalhada => {
//         new ListaDetalhada( listaDetalhada.data )
//     } )
//     return ( 
//         <React.Fragment>
//             <h2>{ e.nome }</h2>
//             <img src={ e.thumbUrl } alt={ e.nome }></img>
//             <span>{ `${ e.temporada.toString().padStart(2, '0') }/${ e.ordemEpisodio.toString().padStart(2, '0') }` }</span>
//             <span>{ `${ e.duracao } min` }</span>
//             {/*<span>{ novaLista.then( a => a.sinopse ) }</span>*/}
//         </React.Fragment>
//     )
// }

// export default TelaDetalheEpisodio 

export default class TelaDetalhadaEpisodio extends Component {
   
    constructor(props) {
        super(props)
        this.episodiosApi = new EpisodiosApi()
        this.state = {
            listaDetalhada: {}
        }
    }
 
    componentDidMount() {
        this.episodiosApi.buscarDetalhes(this.props.location.state.e.id)
            .then(detalhesDoServidor => {
                this.setState({
                    listaDetalhada: detalhesDoServidor.data[0]
                })
            })
    }

    render() {
        const { e } = this.props.location.state
        const { listaDetalhada } = this.state
        return( 
            <React.Fragment>
                <h2>{ e.nome }</h2>
                <img src={ e.thumbUrl } alt={ e.nome }></img>
                 <span>{ `${ e.temporada.toString().padStart(2, '0') }/${ e.ordemEpisodio.toString().padStart(2, '0') }` }</span>
                <span>{ `${ e.duracao } min` }</span>
                <span>{ listaDetalhada.sinopse }</span>
                <span>{ listaDetalhada.dataEstreia }</span>
                <span>{ e.nota }</span>
                <span>{ listaDetalhada.notaImdb }</span>
            </React.Fragment>
         )
    }
}
