import React from 'react'
import { Link } from 'react-router-dom'

function ordemDefinitiva( a, b ) {
    const resultadoTemporario = a.temporada - b.temporada
    return resultadoTemporario === 0 ? a.ordemEpisodio - b.ordemEpisodio : resultadoTemporario
} 

const ListaAvaliacoes = props => {
  const { listaEpisodios } = props.location.state
  const temporadaParaOrdena = listaEpisodios.avaliados.sort( ordemDefinitiva )
  return temporadaParaOrdena.map( e => <Link className="link" to={{ pathname: "/episodio", state: { e } }} value={ e.id }><li key={ e.id }>{ `${ e.nome } - ${ e.nota } - ${ e.temporada } - ${ e.ordemEpisodio }` }</li></Link> )
}

export default ListaAvaliacoes

