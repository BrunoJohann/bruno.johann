import Episodio from "./episodio";

export default class ListaDetalhada {
  constructor( episodiosDoServidor ) {
    this.todos = episodiosDoServidor.map( e => new Episodio( e.id, e.episodioId, e.notaImdb, e.sinopse, e.dataEstreia) )
  }

}
