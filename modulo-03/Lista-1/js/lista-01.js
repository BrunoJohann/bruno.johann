var hello = "hello"

//Exercicio-01--------------------------------------------------------------------------------

var circuloArea = { raio: 3, tipo: "A" }
var circuloCircunferencia = {raio: 3, tipo: "C"}

function calcularCirculo( circulo ) {
    if(circulo.tipo == "A"){
        return Math.PI * Math.pow(circulo.raio,2)
    } else if(circulo.tipo == "C"){
        return Math.PI * (circulo.raio+circulo.raio)
    } else {
        return false;
    }
}

//Exercicio-02-----------------------------------------------------------------------------------

function naoBissexto(ano) {
    if(ano%4 === 0){
        return "Ano bissexto"
    } else if (ano === undefined) {
        return "Nao informou ano"
    } else {
        return "Ano nao bissexto"
    }
}

//Exercicio-03-----------------------------------------------------------------------------------

var array = [ 1, 56, 4.34, 6, -2 ]

function somarPares(array) {
    var soma = 0
    for(var i = 0; i < array.length; i+= 2) {
        soma += array[i]
    }
    return soma
}

//Exercicio-04-----------------------------------------------------------------------------------

function adicionar(x) {
    return function(y) { 
        return function(z){ return x + y + z } 
    };
}

var adicionarDeOutraForma = x => y => x + y

//Exercicio-05-----------------------------------------------------------------------------------

function imprimirBRL(numero) {
    numero = numero.toFixed(2).split('.')
    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.')
    if(numero[0][0] === "-") {
        return "-R$ " + numero.join(',').replace('-', '')
    } else {
    return "R$ " + numero.join(',')
    }
}

function imprimirBRLDeOutraForma(numero) {
    let array = numero.toFixed(2).toString().split('.')
    let arrayInvertido = []
    let arrayFinal = []
    let segundoArray = ''
    let sinalPositivo = true
    let tamanhoMax = array[0].length
    let primeiroContador = 0
    let segundoContador = 0
    
    if(array[0] === "0" && array[1] === "00") {
        return "RS 0,00"
    }
    if(array[0][0] === '-') {
        sinalPositivo = false
    }
    for(let i = 0; i < tamanhoMax; i++) {
        arrayInvertido.push(array[0][tamanhoMax -i -1])
    }

    arrayInvertido = arrayInvertido.join("")

    for(let i = 0; i < tamanhoMax; i++) {
        segundoArray += arrayInvertido[i]
        primeiroContador++
        if(primeiroContador % 3 === 0 && primeiroContador !== tamanhoMax){
            segundoContador++
            segundoArray += '.'
        }
    }

    tamanhoMax += segundoContador
    for(let i = 0; i < tamanhoMax; i++) {
        arrayFinal.push(segundoArray[tamanhoMax -i -1])
    }

    if(sinalPositivo) {
      return "R$ " + [arrayFinal.join(''), array[1]].join(',')
    } else {
      return "-R$ " + [arrayFinal.join('').replace('-', ''), array[1]].join(',')
    }
}




