// var luke = {
//   nome: "Luke",
//   atacarComSabre: function() {
//     console.log( `${ this.nome } atacar com sabre!` )
//   }
// }

// var luke = new Jedi('Luke')
// luke.atacarComSabre()
// var maceWindu = new Jedi('Mace Windu')
// maceWindu.atacarComSabre()

// function Jedi(nome) {
//   this.nome = nome
// }

// Jedi.prototype.atacarComSabre = function() {
//   console.log( `${ this.nome } atacar com sabre!` )
// }

class Jedi {
  constructor(nome) {
    this.nome = nome
  }

  // problema de undefined, pois utiliza this da window
  atacarComSabre() {
    setTimeout( function() {
      console.log( `${ this.nome } atacar com sabre!` )
    }, 1000 )
  }

  atacarComBind() {
    setTimeout( function() {
      console.log( `${ this.nome } atacar com sabre!` )
    }.bind(this), 1000 )
  }

  atacarComSelf() {
    // var that = this
    // var thiz = this
    var self = this
    setTimeout( function() {
      console.log( `${ self.nome } atacar com sabre!` )
    }, 1000 )
  }

  atacarComArrowFunctions() {
    setTimeout( () => {
      console.log( `${ this.nome } atacar com sabre!` )
    }, 1000 )
  }

}

var luke = new Jedi('Luke')
luke.atacarComSabre()
luke.atacarComArrowFunctions()
var maceWindu = new Jedi('Mace Windu')
maceWindu.atacarComSabre()
maceWindu.atacarComArrowFunctions()


String.prototype.shimeji = function() { return this + "Shimeji!!"}