class Pokemon { // eslint-disable-line no-unused-vars
  constructor( objVindoDaApi ) {
    this.nome = objVindoDaApi.name
    this.thumbUrl = objVindoDaApi.sprites.front_default
    this._altura = objVindoDaApi.height
    this._peso = objVindoDaApi.weight
    this._tipo = objVindoDaApi.types.map( t => t.type.name )
    this._stats = objVindoDaApi.stats.map( s => s.base_stat )
    this._id = objVindoDaApi.id
  }

  get altura() {
    return this._altura * 10
  }

  get alturaEmCm() {
    return `Altura: ${ this.altura } cm`
  }

  get peso() {
    return this._peso / 10
  }

  get tipo() {
    return this._tipo
  }

  get stats() {
    return this._stats
  }

  get id() {
    return this._id
  }
}
