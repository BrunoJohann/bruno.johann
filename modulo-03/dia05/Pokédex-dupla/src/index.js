const elem = document.getElementById( 'pesquisa-pokemon' );
let pokemonAnterior = elem.value

function renderizaPokemonNaTela( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' )
  const nome = dadosPokemon.querySelector( '.nome' )
  nome.innerText = pokemon.nome
  const imgPokemon = dadosPokemon.querySelector( '.thumb' )
  imgPokemon.src = pokemon.thumbUrl
  const alturaPokemon = dadosPokemon.querySelector( '.altura' )
  alturaPokemon.innerText = pokemon.alturaEmCm
  const pesoPokemon = dadosPokemon.querySelector( '.peso' )
  pesoPokemon.innerText = `Peso: ${ pokemon.peso } Kg`
  const tipoEscritoPokemon = dadosPokemon.querySelector( '.tipoEscrito' )
  tipoEscritoPokemon.innerText = 'Tipo: '
  for ( let i = 0; i < 2; i += 1 ) {
    const tipoPokemon = dadosPokemon.querySelector( `.tipo${ i }` )
    const nenhumTipo = pokemon.tipo[ i ] === undefined
    if ( nenhumTipo ) {
      tipoPokemon.innerText = ''
    } else {
      tipoPokemon.innerText = pokemon.tipo[ i ]
    }
  }

  let statusPokemon = dadosPokemon.querySelector( '.speed' )
  statusPokemon.innerText = `Speed: ${ pokemon.stats[0] }%`
  statusPokemon = dadosPokemon.querySelector( '.special-defense' )
  statusPokemon.innerText = `Special-defense: ${ pokemon.stats[1] }%`
  statusPokemon = dadosPokemon.querySelector( '.special-attack' )
  statusPokemon.innerText = `Special-attack: ${ pokemon.stats[2] }%`
  statusPokemon = dadosPokemon.querySelector( '.defense' )
  statusPokemon.innerText = `Defense: ${ pokemon.stats[3] }%`
  statusPokemon = dadosPokemon.querySelector( '.attack' )
  statusPokemon.innerText = `Attack: ${ pokemon.stats[4] }%`
  statusPokemon = dadosPokemon.querySelector( '.hp' )
  statusPokemon.innerText = `Hp: ${ pokemon.stats[5] }`
  const idPokemon = dadosPokemon.querySelector( '.id' )
  idPokemon.innerText = `Id: ${ pokemon.id }`
}

function buscarPokemonPorId( id ) {
  const pokeApi = new PokeApi()
  if ( pokemonAnterior !== id ) {
    if ( id > 0 && id < 803 ) {
      pokemonAnterior = id
      pokeApi.buscar( id )
        .then( pokemonServidor => {
          const poke = new Pokemon( pokemonServidor )
          renderizaPokemonNaTela( poke )
        } )
    } else {
      elem.value = 'Digite um id válido'
    }
  }
}

function randomizar() {
  const numero = Math.floor( Math.random() * 802 + 1 )
  buscarPokemonPorId( numero )
}
