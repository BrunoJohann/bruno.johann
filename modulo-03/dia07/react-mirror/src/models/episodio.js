export default class Episodio {
  constructor( nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido ) {
    this.nome = nome
    this.duracao = duracao
    this.temporada = temporada
    this.ordemEpisodio = ordemEpisodio
    this.thumbUrl = thumbUrl
    this.qtdVezesAssistido = qtdVezesAssistido || 0
    // C#: this.qtdVezesAssistido = qtdVezesAssistido ?? 0
  }

  validarNota =  ( nota ) => {
    nota = parseInt( nota )
    return 1 <= nota && nota <= 5
  }

  avaliar( nota ) {
    this.nota = parseInt( nota )
    //var checarNota = parseInt( nota )
    //this.nota = (checarNota > 0 && checarNota < 6) ? checarNota : ''
    this.assistido = true
  }

  get duracaoEmMin() {
    return `${ this.duracao }min`
  }

  get temporadaEpisodio() {
    return `${ this.temporada.toString().padStart(2, '0') }/${ this.ordemEpisodio.toString().padStart(2, '0') }`
  }
}
