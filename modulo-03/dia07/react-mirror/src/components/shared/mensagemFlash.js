import React, { Component } from 'react'
import './mensagemFlash.css'
import PropTypes from 'prop-types'

export default class MensagemFlash extends Component {

    constructor( props ) {
        super( props )
        this.idsTimeouts = []
        this.animacao = ''
    }
    
    limparTimeouts() {
        this.idsTimeouts.forEach( c => clearTimeout( c ) )
    }
    
    componentWillUnmount() {
        this.limparTimeouts()
    }
    
    componentDidUpdate( prevProps ) {
        const { deveExibirMensagem, segundos } = this.props
        if ( prevProps.deveExibirMensagem !== deveExibirMensagem ) {
            this.limparTimeouts()
            const novoIdTimeout = setTimeout ( () => {
                this.fechar()
            }, segundos * 1000 )
            this.idsTimeouts.push( novoIdTimeout )
        }
    }
    
    fechar = () => {
        console.log(`Fechar`)
        this.props.atualizarMensagem( false )
    }

    render() {
        const { deveExibirMensagem, mensagem, cor } = this.props

        if ( this.animacao || deveExibirMensagem ) {
            this.animacao = deveExibirMensagem ? 'fade-in' : 'fade-out'
        }

        return  <span onClick={ this.fechar } className={ `flash ${ cor } ${ this.animacao }` }>{ mensagem || 'Nota registrada!'}</span>
    }
}

MensagemFlash.propTypes = {
    mensagem: PropTypes.string.isRequired,
    deveExibirMensagem: PropTypes.bool.isRequired,
    atualizarMensagem: PropTypes.func.isRequired,
    cor: PropTypes.oneOf( [ 'verde', 'vermelho' ] ),
    segundos: PropTypes.number
}

MensagemFlash.defaultProps = {
    cor: 'verde',
    segundos: 3
}