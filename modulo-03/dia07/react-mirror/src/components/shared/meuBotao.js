import React from 'react';
import '../../App.css';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const MeuBotao = ( { cor, texto, funcionalidade, link, dadosNavegacao } ) => (
    <React.Fragment>
        { 
            <button className={`btn ${ cor } `} onClick={ funcionalidade }>
                {
                link ? <Link className='link' to={ { pathname: link, state: dadosNavegacao } }>{ texto }</Link> : texto 
                }
            </button>
        }
    </React.Fragment>
)

// const MeuBotao = ( { cor, texto, funcionalidade, link } ) => (
//     funcionalidade ? ( 
//         <React.Fragment>
//             { 
//                 <button className={`btn ${ cor } `} onClick={ funcionalidade }>{ texto }</button>
//             }
//         </React.Fragment>
//     ) : <React.Fragment>
//     { 
//         <button className={`btn ${ cor }`}>
//             <Link className='link ' to={ link }>{ texto }</Link>
//         </button>
//     }
// </React.Fragment>
// )

export default MeuBotao 

// export default class MeuBotao extends Component {
//     render() {
//         //const { texto, cor, funcionalidade } = this.props
//         return this.props.funcionalidade ? (
//             <React.Fragment>
//                     <button className={`btn ${ this.props.cor } `} onClick={ this.props.funcionalidade }>{ this.props.texto }</button>
//             </React.Fragment>
//         ) : <React.Fragment>
//                     <button className={`btn ${ this.props.cor } `} >
//                         <Link to={ link }>{ texto }</Link>
//                     </button>
//             </React.Fragment>
//     }
// }

MeuBotao.propTypes = {
    texto: PropTypes.string.isRequired,
    cor: PropTypes.oneOf( [ 'verde', 'azul' ] ),
    funcionalidade: PropTypes.func,
    link: PropTypes.string,
    dadosNavegacao: PropTypes.object,
}

MeuBotao.defaultProps = {
    cor: 'verde'
}
