import React, { Component } from 'react'

import ListaEpisodios from '../models/listaEpisodios';
import EpisodioUi from '../components/episodioUi';

import MensagemFlash from '../components/shared/mensagemFlash';
import MeuInputNumero from '../components/shared/meuInputNumero';
import MeuBotao from '../components/shared/meuBotao';

export default class PaginaIncial extends Component {

constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // definindo estado inicial de um componente
    this.state = {
        episodio: this.listaEpisodios.episodioAleatorio,
        deveExibirMensagem: false,
        mensagem: '',
        deveExibirErro: false
    }
}

sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState( {
        episodio, deveExibirMensagem: false
    } )
}

marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
        episodio,
    } )
}

exibeMensagem = ( {cor, mensagem} ) => {
    this.setState( {
        deveExibirMensagem: true,
        cor,
        mensagem
    } )
}

registrarNota = ( { valor, erro } ) => {
    if ( erro ) {
        // se tiver erro no campo, atualizamos estado com exibição e já retornamos,
        // para não rodar o código como se estivesse válida a obrigatoriedade
        return this.setState( {
        deveExibirErro: erro
        } )
    } else {
        this.setState( {
        deveExibirErro: erro
        } )
    }

    const { episodio } = this.state
    let cor, mensagem
    if ( episodio.validarNota( valor ) ) {
        episodio.avaliar( valor )
        cor = 'verde'
        mensagem = 'Registramos sua nota!'
    } else {
        cor = 'vermelho'
        mensagem = 'Informar uma nota válida (entre 1 e 5)'
    }
    this.exibeMensagem( { cor, mensagem } )

}

atualizarMensagem = devoExibir => {
    this.setState( {
        deveExibirMensagem: devoExibir
    } )
}

render() {
    const { episodio, deveExibirMensagem, mensagem, cor, deveExibirErro } = this.state
    const { listaEpisodios } = this
    return (
        <React.Fragment>
            <MensagemFlash atualizarMensagem={ this.atualizarMensagem } deveExibirMensagem={ deveExibirMensagem } mensagem={ mensagem } cor={ cor } segundos={ 5 }/>
            <EpisodioUi episodio={ episodio }/>
            <MeuInputNumero 
                placeholder='1 a 5' 
                visivel={ episodio.assistido || false } 
                mensagemCampo='Qual sua nota para este episódio?'
                atualizarValor={ this.registrarNota }
                deveExibirErro={ deveExibirErro }
                obrigatorio={ true }/>
            <div className="botoes">
                <MeuBotao texto={ 'Próximo' } cor={ 'verde' } funcionalidade= { this.sortear }/>
                <MeuBotao texto={ 'Já assisti' } cor={ 'azul' } funcionalidade= { this.marcarComoAssistido }/>
                <MeuBotao texto={ 'Pagina avaliações' } cor={ 'verde' } link={ "/avaliacoes" } dadosNavegacao={ { listaEpisodios } }/>
            </div>
        </React.Fragment>
        )
    }
}