import React from 'react'

const ListaAvaliacao = props => {
  const { listaEpisodios } = props.location.state
  return listaEpisodios.avaliados.map( e => {
    return <li>{ `${ e.nome } - ${ e.nota }` }</li>
  } )
}

export default ListaAvaliacao
