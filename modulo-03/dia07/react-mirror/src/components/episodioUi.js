import React, { Component } from 'react'

export default class EpisodioUi extends Component {
    render() {
        const { episodio } = this.props
        return (
          <React.Fragment>
            <h2>{ episodio.nome }</h2>
            <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
            <span>Já assisti? { episodio.assistido ? 'sim' : 'não' }, { episodio.qtdVezesAssistido } vez(es)</span>
            <span>{ episodio.duracaoEmMin }</span>
            <span>{ episodio.temporadaEpisodio }</span>
            <span>{ episodio.nota || 'sem nota' }</span>
          </React.Fragment>
        )
    }
}