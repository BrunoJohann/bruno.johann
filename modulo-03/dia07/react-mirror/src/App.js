import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';

import PaginaIncial from './components/paginaInicial';
import ListaAvaliacao from './components/listaAvaliacao'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <header className="App-header">
              <Route path="/" exact component={ PaginaIncial }/>
              <Route path="/avaliacoes" component={ ListaAvaliacao }/>
            </header>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default App;

//const ListaAvaliacao = () => <h2>Lista de avaliação</h2> 