

public class TradutorParaAlemao implements Tradutor{
    public String traduzir(String textoEmPortugues) {
        switch(textoEmPortugues) {
            case "Sim":
                return "Ya";
            case "Obrigado":
                return "Danke";
            default :
                return null;
        }
        
    }
}
