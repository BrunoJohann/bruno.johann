import java.util.*;

public class AgendaContatos{
    private HashMap<String, String> contatos;   

    public AgendaContatos() {
        contatos = new LinkedHashMap<>();
    }

    public void adicionar(String contato, String telefone) {
        contatos.put(contato, telefone);
    }

    public String consultar(String contato) {
        return contatos.get(contato);
    }

    public String consultarContato(String telefone) {
        for (HashMap.Entry<String, String> par : contatos.entrySet()) {
            if (par.getValue().equals(telefone)) {
                return par.getKey();
            }
        }
        return null;
    }

    public String csv() {
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for (HashMap.Entry<String, String> par : contatos.entrySet()) {
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador);
            builder.append(contato);
        }

        return builder.toString();
    }
   
}
