import java.util.ArrayList;

public class PaginadorInventario {

    private Inventario mochila;
    private int marcador;
    
    public PaginadorInventario(Inventario inventario){
        this.mochila = inventario;
    }
    
    public void pular(int marcador){
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
    public ArrayList<Item> limitar(int qtd){
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador + qtd;
        for(int i = this.marcador; i < fim && i < this.mochila.getItens().size(); i++) {
            subConjunto.add(this.mochila.getPosicao(i));
        }
        return subConjunto;
    }
    
}



