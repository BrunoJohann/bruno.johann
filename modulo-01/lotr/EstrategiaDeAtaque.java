import java.util.ArrayList;

public interface EstrategiaDeAtaque {
   ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);
}
