
import java.util.*;

public class ExercitoDeElfos {
    protected final static ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(
                ElfosVerdes.class,
                ElfoNoturno.class
            )
        );
    protected ArrayList<Elfo> elfos = new ArrayList<>();
    protected HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>(); 

    public void alistar(Elfo elfo) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if (podeAlistar) {
            elfos.add(elfo);
            ArrayList<Elfo> elfosDoStatus = porStatus.get(elfo.getStatus());
            if (elfosDoStatus == null) {
                elfosDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfosDoStatus);
            }
            elfosDoStatus.add(elfo);
        }
    }

    public ArrayList<Elfo> buscar(Status status) {
        return this.porStatus.get(status);
    }

    public ArrayList<Elfo> getElfos() {
        return this.elfos;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        for(int i = 0; i < atacantes.size(); i++){
            for(int j = 0; j < atacantes.size() - 1; j++){
                Elfo atual = atacantes.get(j);
                Elfo proximo = atacantes.get(j + 1);
                boolean primeiros = atual.getClass().equals(ElfoNoturno.class);
                if(primeiros){
                    Elfo elfoTrocado = atual;
                    atacantes.set(j, proximo);
                    atacantes.set(j + 1, elfoTrocado);
                }
              
            }
            if(atacantes.get(i).getStatus().equals(Status.MORTO)){
                    atacantes.remove(i);
            }
        }
        return elfos = atacantes;
    }
    
    public ArrayList<Elfo> exercitoMeioAMeio() {
        ArrayList<Elfo> listaFiltrada = getOrdemDeAtaque(this.elfos);
        int qtdVerde = 0;
        int qtdNoturno = 0;
        int diferenca = 0;
        boolean verificaElfo;
        for(Elfo elfo : listaFiltrada){
            if(elfo.getClass().equals(ElfosVerdes.class)){
                qtdVerde++;
            }else{
                qtdNoturno++;
            }
        }
        
        if(qtdVerde > qtdNoturno){
            // qtdVerde = qtdNoturno;
            diferenca = qtdVerde - qtdNoturno;
            verificaElfo = true;
        }else{
            // qtdNoturno = qtdVerde;
            diferenca = qtdNoturno - qtdVerde;
            verificaElfo = false;
        }
        
        if(verificaElfo){
            for(int i = 0; i < diferenca; i++) {
                listaFiltrada.remove(i);
            }
        } else {
            for(int i = 0; i < diferenca; i++) {
                listaFiltrada.remove(listaFiltrada.size() - 1 - i);
            }
        }
        
        return elfos = listaFiltrada;
    }
    
    public void ataqueIntercalado(Elfo elfoPrimeiroAtaque) {
        boolean primeiroAtaque = 
            elfoPrimeiroAtaque.getClass().equals(ElfosVerdes.class);
        
        if(primeiroAtaque){
            
            for(int i = 0; i < elfos.size(); i++) {
                
            }
        }
        
    }
    
}










