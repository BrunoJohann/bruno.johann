import java.util.ArrayList;
import java.util.Arrays;

public class ElfoDaLuz extends Elfo
{
    private int qtdAtaques = 0;
    private final double QTD_VIDA_GANHA = 10;
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
            Arrays.asList("Espada de Galvorn")
        );
    
    public ElfoDaLuz(String nome) {
        super(nome);   
        this.qtdDano = 21.0;
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
    }
    
    public boolean devePerderVida() {
        return qtdAtaques % 2 == 1;
    }
    
    public void ganharVida() {
        vida += QTD_VIDA_GANHA;
    }
    
    public void perderItem(Item item) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
    }
    
    private Item getEspada() {
        return this.getInventario().buscarPeloNome(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    public void atacarComEspada(Dwarf dwarf) {
        Item espada = getEspada();
        // esse if nao e' mais necessario desde que corrigimos com o ItemSempreExistente, mas deixei aqui pra ficar claro (para aqueles que nao implementaram o cenario de testes que alterava a quantidade)
        if (espada.getQuantidade() > 0) {
            qtdAtaques++;
            dwarf.sofrerDano();
            if (devePerderVida()) {
                sofrerDano();
            } else {
                ganharVida();
            }
        }
    }
    
    
}
