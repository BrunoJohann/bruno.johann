
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    
    private final double DELTA = 1e-9;
    
    @After
    public void TearDown() {
        System.gc();
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Barindrod Shatterdigger");
        // Act
        umElfo.atirarFlecha(dwarf);
        // Assert
        assertEquals(1, umElfo.getExperiencia());
        // // Lei de Demeter - Law of Demeter
        // //assertEquals(41, umElfo.getFlecha().getQuantidade());
        assertEquals(1, umElfo.qtdFlechas());
    }

    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf dwarf = new Dwarf("Barindrod Shatterdigger");
        // Act
        umElfo.atirarFlecha(dwarf);
        umElfo.atirarFlecha(new Dwarf("Kronabela Merryarm"));
        umElfo.atirarFlecha(new Dwarf("Weramoren Lightbreaker"));
        // Assert
        assertEquals(2, umElfo.getExperiencia());
        // Lei de Demeter - Law of Demeter
        //assertEquals(41, umElfo.getFlecha().getQuantidade());
        assertEquals(0, umElfo.qtdFlechas());
    }

    @Test
    public void elfosNascemCom2Flechas() {
        Elfo umElfo = new Elfo("Elrond");
        assertEquals(2, umElfo.qtdFlechas());
    }
    
    @Test
    public void atirarFlechaEmDwarfTiraVida() {
        // Arrange
        Elfo umElfo = new Elfo("Alanis Erna");
        Dwarf dwarf = new Dwarf("Skatmok Greatjaw");
        // Act
        umElfo.atirarFlecha(dwarf);
        // Assert
        assertEquals(100.0, dwarf.getVida(), .00001);
        assertEquals(1, umElfo.getExperiencia());
        assertEquals(1, umElfo.qtdFlechas());
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado() {
        Elfo elfo = new Elfo("Erendriel Inajeon");
        assertEquals(Status.RECEM_CRIADO, elfo.getStatus());
    }
    
    @Test
    public void elfosNascemCom100DeVida() {
        Elfo legolas = new Elfo("Legolas");
        assertEquals(100.0, legolas.getVida(), DELTA);
    }
    
    @Test
    public void contadorDeElfos() {
        
        Elfo legolas = new Elfo("Legolas");
        Elfo legolas1 = new Elfo("Legolas");
        
        int resposta = Elfo.contadorElfos();
        
        assertEquals(2, resposta);
    }
    
    
}