
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;

public class InventarioTest{

    // @Test
    // public void checarPosicoes() {
         // //Arrenge
        // Inventario inventario = new Inventario();
        // Item espada = new Item(1, "Espada");
        // //act
        // inventario.adicionar(espada);
        // inventario.adicionar(espada);
        // //Asset
        // assertEquals(10, inventario.);
    // }
    
    @Test
    public void adicionarUmItem() {
        //Arrenge
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        //Act
        inventario.adicionar(espada);
        //Assert
        assertEquals(espada, inventario.getPosicao(0));
    }

    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(3, "Escudo de aço");
        
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        assertEquals(espada, inventario.getPosicao(0));
        assertEquals(escudo, inventario.getPosicao(1));
    }
    
    @Test
    public void adicionarDoisItensComEspaçoParaUmNaoAdicionaSegundo() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        
        inventario.adicionar(espada);
        inventario.adicionar(armadura);
        
        assertEquals(espada, inventario.getPosicao(0));
        assertEquals(2, inventario.listaSize());
    }
    

    @Test
    public void obterItemNaPrimeiraPosicao() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        
        inventario.adicionar(espada);
        
        assertEquals(espada, inventario.getPosicao(0));
    }
    

    @Test
    public void obterItemNaoAdicionado() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.remover(0);
        
        assertEquals(escudo , inventario.getPosicao(0));
    }

    @Test
    public void removerItem() {
        Inventario inventario = new Inventario(10);
        Item espada = new Item(1, "Espada");
        
        inventario.adicionar(espada);
        inventario.remover(0);
        
        assertNull(inventario.getPosicao(0));
    }

    @Test
    public void removerItemAntesDeAdicionarProximo() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item armadura = new Item(1, "Armadura");
        
        inventario.adicionar(espada);
        inventario.remover(0);
        inventario.adicionar(armadura);
        
        assertEquals(armadura, inventario.getPosicao(0));
        assertEquals(1, inventario.listaSize());
    }

    @Test
    public void adicionarAposRemover() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item bracelete = new Item(1, "Bracelete de prata");
        Item armadura = new Item(1, "Armadura");
        
        inventario.adicionar(espada);
        inventario.adicionar(bracelete);
        inventario.remover(0);
        inventario.adicionar(armadura);
        
        assertEquals(armadura, inventario.getPosicao(1));
        assertEquals(bracelete, inventario.getPosicao(0));
     }
    
    @Test
    public void getDescricoesVariosItens() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item botas = new Item(2, "botas");
        
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(botas);
        
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Escudo,botas", resultado);
    }
    
    @Test
    public void getDescricoesItensRemovendoItemNoMeio() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item flechas = new Item(3, "Flechas");
        Item botas = new Item(1, "Botas de ferro");
    
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.adicionar(botas);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(flechas);
        inventario.adicionar(botas);
    
        inventario.remover(5);
        inventario.remover(3);
        inventario.remover(2);
        inventario.remover(1);
        
        String resultado = inventario.getDescricoesItens();
        assertEquals("Espada,Espada,Flechas,Botas de ferro", resultado);
    }
    
    @Test
    public void getDescricoesItensVazio() {
        Inventario inventario = new Inventario();
        String resultado = inventario.getDescricoesItens();
            
        assertEquals("", resultado);
    }
    
    @Test
    public void getItemMaiorQuantidadeComVarios() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(1, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo de madeira");
        
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(espada, resultado);
    }
        
    
    @Test
    public void getItemMaiorQuantidadeInventarioVazio() {
        Inventario inventario = new Inventario();
        Item resultado = inventario.getItemComMaiorQuantidade();
        
        assertNull(resultado);
    }
    
    @Test
    public void getItemMaiorQuantidadeItensComMesmaQuantidade() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(3, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo de madeira");
        
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        Item resultado = inventario.getItemComMaiorQuantidade();
        assertEquals(lanca, resultado);
    }
    
    @Test
    public void buiscarPeloNomeInventarioVazio() {
        Inventario inventario = new Inventario();
        Item espada = new Item(3, "Espada de aço valiriano");
        
        Item resultado = inventario.buscarPeloNome("Espada de aço valiriano");
        
        assertNull(resultado);
    }
    
    @Test
    public void buscandoObjetoItemPorDescricao() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(3, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo");
        
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        Item resultado = inventario.buscarPeloNome("Espada de aço valiriano");
        
        assertEquals(espada, resultado);
    }
    
    @Test
    public void buscandoVariosObjetosItensPorDescricao() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(3, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo");
        
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        Item resultadoEspada = inventario.buscarPeloNome(new String("Espada de aço valiriano"));
        Item resultadoEscudo = inventario.buscarPeloNome(new String("Escudo"));
        
        assertEquals(espada, resultadoEspada);
        assertEquals(escudo, resultadoEscudo);
    }
    
    @Test
    public void buscandoObjetoItemPorDescricaoQueNaoExiste() {
        Inventario inventario = new Inventario();
        Item lanca = new Item(3, "Lança");
        Item espada = new Item(3, "Espada de aço valiriano");
        Item escudo = new Item(2, "Escudo");
        
        inventario.adicionar(lanca);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        Item resultado = inventario.buscarPeloNome("Botas");
        
        assertEquals(null, resultado);
    }
    
    @Test
    public void metodoInverterInventarioVazio() {
        Inventario inventario = new Inventario();
        
        
    }
    
    @Test
    public void inverterComApenasUmItem() {
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        
        inventario.adicionar(espada);
        ArrayList<Item> resultado = inventario.inverter();
        
        assertEquals(espada, resultado.get(0));
    }
    
    @Test
    public void testandoMetodoInverter() {
        Inventario inventario = new Inventario();
        Item espada = new Item("Espada", 1);
        Item escudo = new Item("Escudo", 2);
        
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        ArrayList<Item> resultado = inventario.inverter(); // retorna o item Escudo e depois Espada
        // Item resultado = inventario.getPosicao(0); // retorna os itens Espada, Escudo
    
        assertEquals(escudo,resultado.get(0));
        assertEquals(espada, resultado.get(1));
    }
    
    @Test
    public void testandoMetodoInverterComItensDeValoresIguais() {
        Inventario inventario = new Inventario();
        Item espada1 = new Item("Espada", 1);
        Item espada2 = new Item("Espada", 1);
        
        inventario.adicionar(espada1);
        inventario.adicionar(espada2);
        
        ArrayList<Item> resultado = inventario.inverter(); // retorna o item Escudo e depois Espada
        // Item resultado = inventario.getPosicao(0); // retorna os itens Espada, Escudo
    
        assertEquals(espada2,resultado.get(0));
        assertEquals(espada1, resultado.get(1));
    }
    
    @Test
    public void testandoOrdenarItens() {
        Inventario mochila = new Inventario();
        Item espada = new Item(6, "Espada");
        Item botas = new Item(7, "Botas");
        Item escudo = new Item(2, "Escudo");
        Item balas = new Item(4, "Balas");
        
        mochila.adicionar(espada);
        mochila.adicionar(botas);
        mochila.adicionar(escudo);
        mochila.adicionar(balas);
        mochila.ordenarItens();
        
        String resultado = mochila.getDescricoesItens();
        
        assertEquals("Escudo,Balas,Espada,Botas" , resultado);
    }
    
    @Test
    public void testandoOrdenarItensDescendente() {
        Inventario mochila = new Inventario();
        Item espada = new Item(6, "Espada");
        Item botas = new Item(7, "Botas");
        Item escudo = new Item(2, "Escudo");
        Item balas = new Item(4, "Balas");
        
        mochila.adicionar(espada);
        mochila.adicionar(botas);
        mochila.adicionar(escudo);
        mochila.adicionar(balas);
        mochila.ordenarItens(TipoOrdenacao.DESC);
        
        String resultado = mochila.getDescricoesItens();
        
        assertEquals("Botas,Espada,Balas,Escudo" , resultado);
    }
}