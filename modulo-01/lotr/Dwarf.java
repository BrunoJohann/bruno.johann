public class Dwarf extends Personagem{
    private boolean escudoEquipado = false;
    
    public Dwarf(String nome){
        super(nome);
        this.qtdDano = 10.0;
        this.vida = 110.0;
        Item escudo = new Item(1, "Escudo");
        this.mochila = new Inventario();
        this.mochila.adicionar(escudo);
    }
    
    public boolean escudosEmUso() {
        return this.escudoEquipado;
    }
    
    // private boolean dwarfMorreu(){
        // //return getVida() > 0;
        // return this.vida > 0;
    // }
    
    // public void sofrerDano() {
        // if(dwarfMorreu()){
            // double qtdDano = this.escudoEquipado ? 5.0 : 10.0;
            // this.status = Status.SOFREU_DANO;
            // this.vida -= qtdDano;
        // } else if(dwarfMorreu() == false){
            // dwarfEstaDefinitivamenteMorto();
        // } 
    // }
    
    public double calcularDano() {
        return this.escudoEquipado ? 5.0 : this.qtdDano;
    }
    
    // public Status dwarfEstaDefinitivamenteMorto() {
        // return this.status = Status.MORTO;
    // }
    
    public void equiparEscudo() {
        String escudo = this.mochila.getPosicao(0).getDescricao();
        int numeroDeEscudos = this.mochila.getPosicao(0).getQuantidade();
        boolean temEscudo = escudo.equals("Escudo");
        boolean escudoMaiorQueZero = numeroDeEscudos > 0;
        if(temEscudo && escudoMaiorQueZero){
            this.escudoEquipado = true;
        }
    }
    
    public String imprimirResumo() {
        return "Dwarf";
    }
    
    
}


















