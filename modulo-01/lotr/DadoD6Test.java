

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DadoD6Test {
    
    @Test
    public void jogarDoisDadosComMesmaSeedGeraMesmoNumero() {
        DadoD6 dado1 = new DadoD6();
        dado1.setSeed(3);

        DadoD6 dado2 = new DadoD6();
        dado2.setSeed(3);

        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
        assertEquals(dado1.sortear(), dado2.sortear());
    }
    
    @Test
    public void jogarDoisDadosComSeedsDiferentesGeraNumerosDiferentes() {
        DadoD6 dado1 = new DadoD6();
        dado1.setSeed(3);

        DadoD6 dado2 = new DadoD6();
        dado2.setSeed(3000);

        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
        assertNotEquals(dado1.sortear(), dado2.sortear());
    }
    
}
