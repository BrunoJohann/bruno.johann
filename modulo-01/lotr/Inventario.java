import java.util.ArrayList;

public class Inventario {
    
    //private Item[] itens;
    private ArrayList<Item> lista;
    
    public Inventario() {
        this.lista = new ArrayList<>();
    }
    
    public Inventario(int quantidadeItens) {
        this.lista = new ArrayList<>(quantidadeItens);
    }
    
    public void adicionar(Item item) {     
        this.lista.add(item);
    }
    
    public Item getPosicao(int posicao) {
        if(posicao >= this.lista.size()){
            return null;
        }
        return this.lista.get(posicao);
    }
    
    public int listaSize() {
        return this.lista.size();
    }
    
    public void remover(int posicao) {
        this.lista.remove(posicao);
    }
    
    public void remover(Item item) {
        this.lista.remove(item);
    }
    
    // public Item[] getItens() {
        // return this.itens;
    // }
    
    public ArrayList<Item> getItens() {
        return this.lista;
    }
    
    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder();
        for (int i = 0; i < this.lista.size(); i++) {
            Item item = this.lista.get(i);
            if(item != null){
                String descricao = item.getDescricao();
                descricoes.append(descricao);
                boolean deveColocarVirgula = i < this.lista.size() - 1;
                if (deveColocarVirgula) {
                    descricoes.append(",");
                }
            }
        }
        return descricoes.toString();
    }
    
    // public String getDescricoesItens() {
        // StringBuilder descricoes = new StringBuilder();
        // for (int i = 0; i < this.lista.size(); i++) {
            // if (this.lista.get(i) != null) {
                // //String descricao = this.itens[i].getDescricao();
                // String descricao = this.lista.get(i).getDescricao();
                // descricoes.append(descricao);
                // boolean deveColocarVirgula = i < this.ultimaPosicaoPreenchida;
                // if (deveColocarVirgula) {
                    // descricoes.append(",");
                // }
            // }
        // }
        // return descricoes.toString();
    // }
    
    public Item getItemComMaiorQuantidade() {
        int indice = 0, maiorQuantidadeParcial = 0;
        for (int i = 0; i < this.lista.size(); i++) {
            Item item = this.lista.get(i);
            if (item != null) {
                //int qtdAtual = this.itens[i].getQuantidade();
                int qtdAtual = item.getQuantidade();
                if (qtdAtual > maiorQuantidadeParcial) {
                    maiorQuantidadeParcial = qtdAtual;
                    indice = i;
                }
            }
        }
        return this.lista.size() > 0 ? this.lista.get(indice) : null;
    }
    
    public Item buscarPeloNome(String qualItem) {
        //StringBuilder item = new StringBuilder(qualItem);
        for(Item itemAtual : this.lista) {
            boolean encontrei = itemAtual.getDescricao().equals(qualItem);
            if(encontrei) {
                return itemAtual;
            }
        }
        return null;
    }
    
    public ArrayList<Item> inverter()  {
        ArrayList<Item> listaInvertida = new ArrayList<>(this.lista.size());
        for(int i = this.lista.size() - 1; i >=0; i--){
            listaInvertida.add(this.lista.get(i));
        }
        return listaInvertida;
    }
    
    // public void inverter() {
        // for(int i = 0; i < this.lista.size() / 2; i++){
            // Item temporario = this.lista.get(i);
            // this.listaInvertida[i] = this.lista.get(this.lista.size() -1 -i);
            // this.listaInvertida[this.lista.size() -1 -i] = temporario;
        // }
    // }
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
     public void ordenarItens(TipoOrdenacao ordenacao) {
        for (int i = 0; i < this.lista.size(); i++) {
            for (int j = 0; j < this.lista.size() - 1; j++) {
                Item atual = this.lista.get(j);
                Item proximo = this.lista.get(j + 1);
                boolean deveTrocar = 
                    ordenacao == TipoOrdenacao.ASC ? 
                        atual.getQuantidade() > proximo.getQuantidade() : atual.getQuantidade() < proximo.getQuantidade();
                if (deveTrocar) {
                    Item itemTrocado = atual;
                    this.lista.set(j, proximo);
                    this.lista.set(j + 1, itemTrocado);
                }
            }
        }
    }
}