import java.util.ArrayList;
import java.util.Arrays;

public class ElfosVerdes extends Elfo{
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de aço valiriano",
            "Arco de Vidro",
            "Flecha de Video"
        )
    );
    
    public ElfosVerdes(String nome) {
        super(nome);   
        this.qtdDeExperienciaPorAtaque = 2;
    }
    
    public void perderItem(Item item) {
        boolean descricaoValida = 
        DESCRICOES_VALIDAS.contains(item.getDescricao());
        if(descricaoValida){
            this.mochila.remover(item);
        }
    }
    
    public void ganharItem(Item item) { 
        boolean descricaoValida = 
        DESCRICOES_VALIDAS.contains(item.getDescricao());
        if(descricaoValida){
            this.mochila.adicionar(item);
        }
    }
    
}
