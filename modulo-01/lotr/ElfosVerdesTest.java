

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfosVerdesTest {
    
    @Test
    public void elfoVerdeGanha2XpPorUmaFlecha() {
        ElfosVerdes elfosverdes = new ElfosVerdes("Legolas mau");
        elfosverdes.atirarFlecha(new Dwarf("DwarfSparring"));
        
        assertEquals(2, elfosverdes.getExperiencia());
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoInvalida() {
        ElfosVerdes elfosverdes = new ElfosVerdes("Legolas mau");
        Item arcoDeVidro = new Item(2, "Arco de Vidro");
        Inventario mochila = new Inventario();
        mochila.adicionar(arcoDeVidro);
        
        elfosverdes.ganharItem(arcoDeVidro);
        
        assertEquals(arcoDeVidro, 
           elfosverdes.getInventario().buscarPeloNome("Arco de Vidro"));
    }
    
    @Test
    public void elfoVerdeAdicionaItemComDescricaoValida() {
        Elfo celeborn = new ElfosVerdes("Celeborn");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celeborn.ganharItem(arcoDeVidro);
        Inventario inventario = celeborn.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.getPosicao(0));
        assertEquals(new Item(2, "Flecha"), inventario.getPosicao(1));
        assertEquals(arcoDeVidro, inventario.getPosicao(2));
    }

    @Test
    public void elfoVerdePerdeItemComDescricaoValida() {
        Elfo celeborn = new ElfosVerdes("Celeborn");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celeborn.perderItem(arcoDeVidro);
        Inventario inventario = celeborn.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.getPosicao(0));
        assertEquals(new Item(2, "Flecha"), inventario.getPosicao(1));
    }
    
    @Test
    public void elfoVerdeNaoPerdeItemComDescricaoInvalida() {
        Elfo celeborn = new ElfosVerdes("Celeborn");
        Item arco = new Item(1, "Arco");
        celeborn.perderItem(arco);
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        celeborn.ganharItem(arcoDeVidro);
        Inventario inventario = celeborn.getInventario();
        assertEquals(new Item(1, "Arco"), inventario.getPosicao(0));
        assertEquals(new Item(2, "Flecha"), inventario.getPosicao(1));
        assertEquals(arcoDeVidro, inventario.getPosicao(2));
    }
    
}
