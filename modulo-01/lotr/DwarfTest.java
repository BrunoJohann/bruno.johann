

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest
{
    @Test
    public void dwarfNasceCom110DeVida() {
        Dwarf umDwarf = new Dwarf("Jodgrumlin Marblebrand");
        assertEquals(110.0, umDwarf.getVida(), .00001);
    }
    
    @Test
    public void dwarfPerdeDezDeVida() {
        //Arrange
        Dwarf dwarf = new Dwarf("Hodouk Koboldarmour");
        //Act
        dwarf.sofrerDano();
        //Assert
        assertEquals(100.0, dwarf.getVida(), .01);
    }
    
    @Test
    public void dwarfPerdeTodaAVida() {
        //Arrange
        Dwarf dwarf = new Dwarf("Hodouk Koboldarmour");
        //Act
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        //Assert
        assertEquals(0.0, dwarf.getVida(), .01);
    }
    
    @Test
    public void statusDoDwarfAgoraMorto() {
        //Arrange
        Dwarf umDwarf = new Dwarf("Nofric Flintmaul");
        //Act
        for(int i = 0; i < 12; i++){
            umDwarf.sofrerDano();
        }
        //Assert
        assertEquals(Status.MORTO, umDwarf.getStatus());
        assertEquals(0, umDwarf.getVida(), .00001);
    }
    
    @Test
    public void statusDoDwarfDeveTrocarParaSOFREU_DANOQuandoLevaDanoENaoMorre() {
        //Arrange
        Dwarf umDwarf = new Dwarf("Nofric Flintmaul");
        //Act
        umDwarf.sofrerDano();
        //Assert
        assertEquals(Status.SOFREU_DANO, umDwarf.getStatus());
    }
    
    @Test
    public void statusDoDwarfDeveSerRECEM_CRIADOquandoNasceENaolevaDano() {
        //Arrange
        Dwarf umDwarf = new Dwarf("Nofric Flintmaul");
        //Assert
        assertEquals(Status.RECEM_CRIADO, umDwarf.getStatus());
    }
    
    @Test
    public void dwarfDeveEquiparEscudo() {
        Dwarf umDwarf = new Dwarf("Nofric Flintmaul");
        
        umDwarf.equiparEscudo();
        
        assertEquals(true, umDwarf.escudosEmUso());
    }
    
    @Test
    public void dwarfDeveLevarMetadeDeUmDano () {
        Dwarf umDwarf = new Dwarf("Nofric Flintmaul");
        
        umDwarf.equiparEscudo();
        umDwarf.sofrerDano();
        
        assertEquals(105.0, umDwarf.getVida(), .00001);
    }
    
    @Test
    public void dwarfDeveLevarUmDanoComEOutroSemEscudo() {
        Dwarf umDwarf = new Dwarf("Nofric Flintmaul");
        
        umDwarf.sofrerDano();
        umDwarf.equiparEscudo();
        umDwarf.sofrerDano();
        
        assertEquals(95.0, umDwarf.getVida(), .00001);
    }
    
    // @Test
    // public void dwarfPerdeItemUtilizandoClassePersonagem() {
        // Dwarf umDwarf = new Dwarf("Nofric Flintmaul");
        
        // umDwarf.perderItem(umDwarf.mochila.getPosicao(0));
        // umDwarf.equiparEscudo();
        
        // boolean resposta = umDwarf.escudosEmUso();
        
        // assertTrue(resposta);
    // }
    
    
}












