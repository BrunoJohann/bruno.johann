
public class Elfo extends Personagem{
    
    private int indiceFlecha;
    protected int experiencia, qtdDeExperienciaPorAtaque; 
    private static int contadorDeElfosPeloMundo = 0;
    
    {
        this.mochila = new Inventario();
        this.indiceFlecha = 1;
        this.qtdDeExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.vida = 100;
    }
    
    public Elfo(String nome) {
        super(nome);
        super.mochila.adicionar(new Item(1, "Arco"));
        super.mochila.adicionar(new Item(2, "Flecha"));     
        contadorDeElfosPeloMundo += 1;
    }
    
    protected void finalize() throws Throwable {
        Elfo.contadorDeElfosPeloMundo--;
    }
    
    protected void setStatus(Status status) {
        this.status = status;
    }
    
    // public Elfo(String nome, int numeroDeItens) {
        // this.setNome(nome);
        // Inventario mochila = new Inventario(numeroDeItens);
    // }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public Item getFlecha() {
        return this.mochila.getPosicao(this.indiceFlecha);
    }
    
    public int qtdFlechas() {
        return this.getFlecha().getQuantidade();
    }
    
    public String getNome() {
        return super.getNome();
    }
    
    protected void aumentarXp(){
        experiencia = experiencia + this.qtdDeExperienciaPorAtaque;
        // experiencia += this.qtdDeExperienciaPorAtaque;
    }
    
    protected boolean podeAtirarFlecha() {
        return this.getFlecha().getQuantidade() > 0;
    }
    
        private boolean estaVivo() {
        return this.vida > 0;
    }
    
    public String imprimirResumo() {
        return "Elfo";
    }
    
    // protected void sofrerDano() {
        // this.vida -= this.vida >= this.qtdDano ? this.qtdDano : this.vida;
        // this.status = estaVivo() ? Status.SOFREU_DANO : Status.MORTO;
    // }
    
    public void atirarFlecha(Dwarf dwarf) {
        int qtdAtual = this.getFlecha().getQuantidade();
        if(podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual -1);
            this.aumentarXp();
            this.sofrerDano();
            dwarf.sofrerDano();
        }   
    }
    
    public static int contadorElfos() {
        return Elfo.contadorDeElfosPeloMundo;
    }
    
    public boolean equals(Object elfo){
        Elfo outroElfo = (Elfo)elfo;
        return this.nome.equals(outroElfo.getNome()) 
                ||
                this.status.equals(outroElfo.getStatus())
                ||
                this.vida == outroElfo.getVida();
    }
    
    @Override
    public String toString() {
        return "" + this.nome;
    }
    
}



















