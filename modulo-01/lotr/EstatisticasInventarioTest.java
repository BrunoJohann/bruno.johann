

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class EstatisticasInventarioTest
{
    
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario mochila = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(mochila);
        
        double resultado = estatistica.calcularMedia();
        
        assertTrue(Double.isNaN(resultado));
    }
    
    @Test
    public void testandoMedia(){
        Inventario mochila = new Inventario();
        Item espada = new Item(6, "Espada");
        Item botas = new Item(6, "botas");
        Item escudo = new Item(2, "Escudo");
        Item balas = new Item(4, "Balas");
        EstatisticasInventario estatistica = new EstatisticasInventario(mochila);
        
        mochila.adicionar(espada);
        mochila.adicionar(botas);
        mochila.adicionar(escudo);
        mochila.adicionar(balas);
        double resultado = estatistica.calcularMedia();
        
        assertEquals(4.5, resultado, 0);
    }
    
    @Test
    public void testandoMedianaCasoPar() {
        Inventario mochila = new Inventario();
        Item espada = new Item(6, "Espada");
        Item botas = new Item(6, "botas");
        Item escudo = new Item(4, "Escudo");
        Item balas = new Item(2, "Balas");
        EstatisticasInventario estatistica = new EstatisticasInventario(mochila);
        
        mochila.adicionar(espada);
        mochila.adicionar(botas);
        mochila.adicionar(escudo);
        mochila.adicionar(balas);
        double resultado = estatistica.calcularMediana();
        
        assertEquals(5, resultado, 0);
    }
    
    @Test
    public void testandoMedianaCasoImpar() {
        Inventario mochila = new Inventario();
        Item espada = new Item(6, "Espada");
        Item botas = new Item(6, "botas");
        Item escudo = new Item(4, "Escudo");
        EstatisticasInventario estatistica = new EstatisticasInventario(mochila);
        
        mochila.adicionar(espada);
        mochila.adicionar(botas);
        mochila.adicionar(escudo);
        double resultado = estatistica.calcularMediana();
        
        assertEquals(6, resultado, 0);
    }
    
    @Test
    public void testandoQuantidadeDeItensAcimaDaMediaInventarioVazio() {
        Inventario mochila = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(mochila);
        int resultado = estatistica.qtdItensAcimaDaMedia();
        assertEquals(0, resultado);
    }
    
    @Test
    public void testandoQuantidadeDeItensAcimaDaMedia() {
        Inventario mochila = new Inventario();
        Item espada = new Item(6, "Espada");
        Item botas = new Item(6, "botas");
        Item escudo = new Item(4, "Escudo");
        EstatisticasInventario estatistica = new EstatisticasInventario(mochila);
        
        mochila.adicionar(espada);
        mochila.adicionar(botas);
        mochila.adicionar(escudo);
        int resultado = estatistica.qtdItensAcimaDaMedia();
        
        assertEquals(2, resultado);
    }
    
    
}



