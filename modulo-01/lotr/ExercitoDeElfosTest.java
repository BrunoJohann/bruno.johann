

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoDeElfosTest
{
    @Test
    public void podeAlistarElfoVerde() {
        Elfo elfoVerde = new ElfosVerdes("Green Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoVerde);
        assertTrue(exercito.getElfos().contains(elfoVerde));
    }

    @Test
    public void podeAlistarElfoNoturno() {
        Elfo elfoNoturno = new ElfoNoturno("Night Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfoNoturno);
        assertTrue(exercito.getElfos().contains(elfoNoturno));
    }

    @Test
    public void naoPodeAlistarElfo() {
        Elfo elfo = new Elfo("Common Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void naoPodeAlistarElfoDaLuz() {
        Elfo elfo = new ElfoDaLuz("Light Legolas");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(elfo);
        assertFalse(exercito.getElfos().contains(elfo));
    }

    @Test
    public void buscarElfosRecemCriadosExistindo() {
        Elfo recemCriado = new ElfosVerdes("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(recemCriado);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(recemCriado)
            );
        assertEquals(esperado, exercito.buscar(Status.RECEM_CRIADO));
    }

    @Test
    public void buscarElfosVivosNaoExistindo() {
        Elfo recemCriadoTrocaPraMorto = new ElfoNoturno("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        // simular morte
        recemCriadoTrocaPraMorto.getFlecha().setQuantidade(7);
        for (int i = 0; i < 7; i++) {
            recemCriadoTrocaPraMorto.atirarFlecha(new Dwarf("Gimli"));
        }
        exercito.alistar(recemCriadoTrocaPraMorto);
        assertNull(exercito.buscar(Status.RECEM_CRIADO));
    }

    @Test
    public void buscarElfosMortosExistindo() {
        Elfo recemCriadoTrocaPraMorto = new ElfoNoturno("Undead Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        // simular morte
        recemCriadoTrocaPraMorto.getFlecha().setQuantidade(7);
        for (int i = 0; i < 7; i++) {
            recemCriadoTrocaPraMorto.atirarFlecha(new Dwarf("Gimli"));
        }
        exercito.alistar(recemCriadoTrocaPraMorto);
        ArrayList<Elfo> esperado = new ArrayList<>(
                Arrays.asList(recemCriadoTrocaPraMorto)
            );
        assertEquals(esperado, exercito.buscar(Status.MORTO));
    }    

    @Test
    public void buscarElfosMortosNaoExistindo() {
        Elfo recemCriado = new ElfosVerdes("Galadriel");
        ExercitoDeElfos exercito = new ExercitoDeElfos();
        exercito.alistar(recemCriado);
        assertNull(exercito.buscar(Status.MORTO));
    }
    
    @Test
    public void elfosNoturnosAtacamPorUltimo(){
        Elfo verdeVivo = new ElfosVerdes("GaladrielVerde");
        Elfo noturnoVivo = new ElfoNoturno("GaladrielNoturno");
        Elfo verdeMorto = new ElfosVerdes("GaladrielMorto");
        Elfo noturnoMorto = new ElfoNoturno("GaladrielMorto");
        Elfo noturnoVivo1 = new ElfoNoturno("BilyNoturno");
        Elfo verdeVivo1 = new ElfosVerdes("JoséVerde");
        verdeVivo.setStatus(Status.RECEM_CRIADO);
        noturnoVivo.setStatus(Status.SOFREU_DANO);
        verdeMorto.setStatus(Status.MORTO);
        noturnoMorto.setStatus(Status.MORTO);
        noturnoVivo1.setStatus(Status.RECEM_CRIADO);
        verdeVivo1.setStatus(Status.SOFREU_DANO);
        
        ArrayList<Elfo> resultado = new ArrayList(
            Arrays.asList(verdeVivo, verdeVivo1, noturnoVivo, noturnoVivo1)
            );
        
        ExercitoDeElfos atacantes = new ExercitoDeElfos();
        atacantes.alistar(verdeVivo);
        atacantes.alistar(noturnoVivo);
        atacantes.alistar(verdeMorto);
        atacantes.alistar(noturnoMorto);
        atacantes.alistar(noturnoVivo1);
        atacantes.alistar(verdeVivo1);
                
        assertEquals(resultado, atacantes.getOrdemDeAtaque(atacantes.getElfos()));
    }
    
    @Test
    public void deveRetornarMeioAMeio(){
        Elfo verdeVivo = new ElfosVerdes("GaladrielVerde");
        Elfo noturnoVivo = new ElfoNoturno("GaladrielNoturno");
        Elfo verdeMorto = new ElfosVerdes("GaladrielMorto");
        Elfo noturnoMorto = new ElfoNoturno("GaladrielMorto");
        Elfo noturnoVivo1 = new ElfoNoturno("BilyNoturno");
        Elfo noturnoVivo2 = new ElfoNoturno("BilyNoturno2");
        Elfo verdeVivo1 = new ElfosVerdes("JoséVerde");
        verdeVivo.setStatus(Status.RECEM_CRIADO);
        noturnoVivo.setStatus(Status.SOFREU_DANO);
        verdeMorto.setStatus(Status.MORTO);
        noturnoVivo1.setStatus(Status.SOFREU_DANO);
        noturnoVivo2.setStatus(Status.RECEM_CRIADO);
        verdeVivo1.setStatus(Status.SOFREU_DANO);
        
        ArrayList<Elfo> resultado = new ArrayList(
            Arrays.asList(verdeVivo, verdeVivo1, noturnoVivo, noturnoVivo1)
            );
        
        ExercitoDeElfos atacantes = new ExercitoDeElfos();
        atacantes.alistar(verdeVivo);
        atacantes.alistar(noturnoVivo);
        atacantes.alistar(verdeMorto);
        atacantes.alistar(noturnoMorto);
        atacantes.alistar(noturnoVivo1);
        atacantes.alistar(verdeVivo1);
        atacantes.alistar(noturnoVivo2);
                
        assertEquals(resultado, atacantes.exercitoMeioAMeio());
    }
}
