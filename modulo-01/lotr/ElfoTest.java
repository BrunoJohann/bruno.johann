import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest
{
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXp() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        // Act
        umElfo.atirarFlecha();
        // Assert
        assertEquals(1, umElfo.getExperiencia());
        // Lei de Demeter - Law of Demeter
        //assertEquals(41, umElfo.getFlecha().getQuantidade());
        assertEquals(1, umElfo.qtdFlechas());
    }
    
    @Test
    public void flechasIniciaisDoElfo(){
        Elfo umElfo = new Elfo("Legolas");
        assertEquals(2, umElfo.qtdFlechas());
    }
    
    @Test
    public void asFlechasDevemAcabar() {
        // Arrange
        Elfo umElfo = new Elfo("Legolas");
        // Act
        umElfo.atirarFlecha();
        umElfo.atirarFlecha();
        umElfo.atirarFlecha();
        // Assert
        assertEquals(2, umElfo.getExperiencia());
        // Lei de Demeter - Law of Demeter
        assertEquals(0, umElfo.qtdFlechas());
    }
}







