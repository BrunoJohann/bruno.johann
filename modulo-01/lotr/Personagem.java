
public abstract class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario mochila;
    protected double vida, qtdDano, vidaGanhaPorDano;
    
    {
        status = Status.RECEM_CRIADO;
        this.mochila = new Inventario();
        this.qtdDano = 0.0;
        this.vidaGanhaPorDano = 0.0;
    }
    
    
    
    protected Personagem(String nome){
        this.nome = nome;
    }
    
    protected void setStatus(Status status) {
        this.status = status;
    }
    
    protected String getNome() {
        return this.nome;
    }
    
    protected void setNome(String nome) {
        this.nome = nome;
    }
    
    protected double getVida() {
        return this.vida;
    }
    
    protected Status getStatus() {
        return this.status;
    }
    
    protected Inventario getInventario() {
        return this.mochila;
    }
    
    protected void ganharItem(Item item) {
        this.mochila.adicionar(item);
    }
    
    protected void perderItem(Item item) {
        // int indice = this.mochila.getItens().indexOf(item);
        this.mochila.remover(item);
    }

    protected double calcularDano() {
        return this.qtdDano;
    }
    
    protected void sofrerDano() {
        this.status = Status.SOFREU_DANO;
        // enquanto tiver vida pra perder, sofre o dano calculado, senão apenas decrementa a quantidade de vida restante para zerá-la.
        this.vida -= this.vida >= this.qtdDano ? calcularDano() : this.vida;
        if (this.vida == 0.0) {
            this.status = Status.MORTO;
        }
    }
    
    protected void ganhaVida() {
        this.vida += this.vidaGanhaPorDano;
    }
    
    public abstract String imprimirResumo();
    
}











