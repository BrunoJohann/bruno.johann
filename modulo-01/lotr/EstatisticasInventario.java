
public class EstatisticasInventario {
    
    private Inventario mochila;
    
    public EstatisticasInventario(Inventario inventario) {
        this.mochila = inventario;
    }
    
    public double calcularMedia(){
        if(this.mochila.getItens().isEmpty()){
            return Double.NaN;
        }
        double media = .0;
        for(Item item : this.mochila.getItens()){
            media += item.getQuantidade();
        }
        return media / mochila.listaSize();
    }
    
    public double calcularMediana() {
        if(this.mochila.getItens().isEmpty()){
            return Double.NaN;
        }
        // double mediana = 0;
        // boolean qtdPar = mochila.listaSize() % 2 == 0;
        // if(mochila.listaSize() % 2 == 0){
            // mediana = mochila.getPosicao(mochila.listaSize()/2).getQuantidade();
            // mediana += mochila.getPosicao((mochila.listaSize()/2) -1).getQuantidade();
            // return mediana/2.0;
        // } 
        // int posicao = (int)((mochila.listaSize()/2) - 0.5);
        // return mochila.getPosicao(posicao).getQuantidade();
        int qtdItens = this.mochila.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.mochila.getPosicao(meio).getQuantidade();
        boolean qtdImpar = qtdItens % 2 == 1;
        if (qtdImpar) {
            return qtdMeio;
        }
        int qtdMeioMenosUm = this.mochila.getPosicao(meio - 1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia() {
        if(this.mochila.getItens().isEmpty()){
            return 0;
        }
        double media = this.calcularMedia();
        int numeroDeItens = 0;
        //int i = 0; i < mochila.listaSize(); i++
        for(Item item : this.mochila.getItens()){
            if(media < item.getQuantidade()){
                numeroDeItens++;
            }
        }
        return numeroDeItens;
    }
    
}
