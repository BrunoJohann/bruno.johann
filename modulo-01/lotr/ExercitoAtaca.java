import java.util.ArrayList;

public class ExercitoAtaca extends ExercitoDeElfos {
    
    private EstrategiaDeAtaque estrategiaDeAtaque;
    
    public ExercitoAtaca(EstrategiaDeAtaque estrategiaDeAtaque) {
        this.estrategiaDeAtaque = estrategiaDeAtaque;
    }    
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        for(int i = 0; i < atacantes.size(); i++){
            for(int j = 0; j < atacantes.size() - 1; j++){
                Elfo atual = atacantes.get(j);
                Elfo proximo = atacantes.get(j + 1);
                boolean primeiros = atual.getClass().equals(ElfoNoturno.class);
                if(primeiros){
                    Elfo elfoTrocado = atual;
                    atacantes.set(j, proximo);
                    atacantes.set(j + 1, elfoTrocado);
                }
              
            }
            if(atacantes.get(i).getStatus().equals(Status.MORTO)){
                    atacantes.remove(i);
            }
        }
        return elfos = atacantes;
    }
}
