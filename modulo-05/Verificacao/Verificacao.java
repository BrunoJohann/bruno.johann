package br.com.dbccompany.Verificacao;

public class Verificacao {
    public static boolean ehGmail(String email){
        return email.matches("(\\w)+((.)(\\w)+)*(@){1}(gmail.com){1}");
    }

    public static void main(String[] args) {
        System.out.println("marlon.brother@hotmail.com: " + ehGmail("marlon.brother@hotmail.com") );
        System.out.println("brunor@gmail.com: " + ehGmail("brunor@gmail.com") );
        System.out.println("gustavo.vidaletti2r@gmail.com: " + ehGmail("gustavo.vidaletti2r@gmail.com") );
    }
}
