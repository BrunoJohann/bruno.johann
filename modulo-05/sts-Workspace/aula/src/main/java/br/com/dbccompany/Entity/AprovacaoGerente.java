package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class AprovacaoGerente {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@OneToMany(mappedBy = "aprovacaoGerente")
	private List<EmprestimoAprovado> emprestimosAprovados = new ArrayList<EmprestimoAprovado>();
	
	@ManyToOne
	private Usuario usuario;
	
	@OneToOne
	@JoinColumn(name = "SOLICITA_EMPRESTIMO")
	private SolicitaEmprestimo solicitaEmprestimo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<EmprestimoAprovado> getEmprestimosAprovados() {
		return emprestimosAprovados;
	}

	public void setEmprestimosAprovados(List<EmprestimoAprovado> emprestimosAprovados) {
		this.emprestimosAprovados = emprestimosAprovados;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public SolicitaEmprestimo getSolicitaEmprestimo() {
		return solicitaEmprestimo;
	}

	public void setSolicitaEmprestimo(SolicitaEmprestimo solicitaEmprestimo) {
		this.solicitaEmprestimo = solicitaEmprestimo;
	}
	
	
}
