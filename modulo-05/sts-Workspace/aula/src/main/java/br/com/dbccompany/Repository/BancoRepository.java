package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;

public interface BancoRepository extends CrudRepository<Banco, Integer>{

	Banco findByCodigo(long codigo);
	Banco findByNome(String nome);
	Banco findByAgencias(List<Agencia> agencias);
	Banco findByAgencias(Agencia agencia);
	
}
