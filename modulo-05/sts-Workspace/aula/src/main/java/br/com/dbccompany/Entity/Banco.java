package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Banco {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name = "CODIGO")
	private long codigo;
	
	@Column(name = "NOME")
	private String nome;
	
	@OneToMany(mappedBy = "banco")
	private List<Agencia> agencias = new ArrayList<Agencia>();
	
	@OneToMany(mappedBy = "banco")
	private List<Usuario> usuarios = new ArrayList<Usuario>();

	

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public long getCodigo() {
		return codigo;
	}



	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}



	public String getNome() {
		return nome;
	}



	public void setNome(String nome) {
		this.nome = nome;
	}



	public List<Agencia> getAgencias() {
		return agencias;
	}



	public void setAgencias(List<Agencia> agencias) {
		this.agencias = agencias;
	}



	public List<Usuario> getUsuarios() {
		return usuarios;
	}



	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}



	@Override
	public String toString() {
		return "Banco [id=" + id + ", codigo=" + codigo + ", nome=" + nome + ", agencias=" + agencias + ", usuarios="
				+ usuarios + "]";
	}
	
	
	
}
