package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class SolicitaEmprestimo {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@OneToOne(mappedBy = "solicitaEmprestimo")
	@JoinColumn(name = "APROVACAO_GERENTE")
	private AprovacaoGerente aprovacaoGenrente;

	@ManyToOne
	private Cliente cliente;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AprovacaoGerente getAprovacaoGenrente() {
		return aprovacaoGenrente;
	}

	public void setAprovacaoGenrente(AprovacaoGerente aprovacaoGenrente) {
		this.aprovacaoGenrente = aprovacaoGenrente;
	}
	
}
