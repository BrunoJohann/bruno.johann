package br.com.dbccompany.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Service.AgenciaService;

@Controller
@RequestMapping("api/agencia")
public class AgenciaController {

	@Autowired
	AgenciaService agenciaService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Agencia> lstAgencia() {
		return agenciaService.allAgencias();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Agencia novaAgencia(@RequestBody Agencia agencia) {
		return agenciaService.salvar(agencia);
	}
	
	@GetMapping(value = "/{codigo}")
	@ResponseBody
	public Agencia agenciaEspecifica(@PathVariable long codigo) {
		return agenciaService.buscarPorCodigo(codigo);
	}
	
	@PutMapping(value = "editar/{id}")
	@ResponseBody
	public Agencia editarAgencia(@PathVariable Integer id, @RequestBody Agencia agencia) {
		return agenciaService.editarAgencia(id, agencia);
	}
	
}
