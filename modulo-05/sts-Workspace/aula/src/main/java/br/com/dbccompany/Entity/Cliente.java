package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name = "NOME")
	private String nome;
	
	@Column(name = "CPF")
	private long CPF;
	
	@Column(name = "RG")
	private long RG;
	
	@Column(name = "SENHA")
	private String senha;
	
	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "cliente_endereco",
            joinColumns = {
            	@JoinColumn(name = "id_cliente")},
            inverseJoinColumns = {
            		@JoinColumn(name = "id_endereco")})
	private List<Endereco> enderecos = new ArrayList<Endereco>();
	
	@OneToMany(mappedBy = "cliente")
	private List<Contato> contatos = new ArrayList<Contato>();
	
	@ManyToMany(mappedBy = "clientes")
	private List<Conta> contas = new ArrayList<Conta>();
	
	@ManyToOne
	private Agencia agencia;
	
	@OneToMany(mappedBy = "cliente")
	@Column(name = "SOLICITACOES_EMPRESTIMO")
	private List<SolicitaEmprestimo> solicitacoesEmprestimo = new ArrayList<SolicitaEmprestimo>();
	
	public long getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getCPF() {
		return CPF;
	}

	public void setCPF(long cPF) {
		CPF = cPF;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getRG() {
		return RG;
	}

	public void setRG(long rG) {
		RG = rG;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}

	public Agencia getAgencia() {
		return agencia;
	}

	public void setAgencia(Agencia agencia) {
		this.agencia = agencia;
	}

	public List<SolicitaEmprestimo> getSolicitacoesEmprestimo() {
		return solicitacoesEmprestimo;
	}

	public void setSolicitacoesEmprestimo(List<SolicitaEmprestimo> solicitacoesEmprestimo) {
		this.solicitacoesEmprestimo = solicitacoesEmprestimo;
	}
	
	
	
}
