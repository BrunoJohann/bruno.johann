package br.com.dbccompany.Entity;

public enum TipoUsuario {
	GERENTE, GERENTE_GERAL, FUNCIONARIO_NORMAL;
}
