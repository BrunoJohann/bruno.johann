package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;

public interface AgenciaRepository extends CrudRepository<Agencia, Integer> {
	
	Agencia findByCodigo(long codigo);
	Agencia findByBanco(Banco banco);
	List<Agencia> findByBanco(List<Agencia> agencias);
	
}
