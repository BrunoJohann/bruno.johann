package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Conta {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@Column(name = "NUMERO")
	private long numero;
	
	@Column(name = "TIPO")
	private String tipo;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "CLIENTE_CONTA",
			joinColumns = {
					@JoinColumn(name = "ID_CONTA")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_CLIENTE")})
	private List<Cliente> clientes = new ArrayList<Cliente>();
	
	@Enumerated(EnumType.STRING)
	@Column(name = "TIPO_CONTA")
	private TipoConta tipoConta;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "CONTA_MOVIMENTACAO",
			joinColumns = {
					@JoinColumn(name = "ID_CONTA")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_MOVIMENTACAO")})
	private List<Movimentacao> movimentacoes = new ArrayList<Movimentacao>();
	
	public long getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public List<Movimentacao> getMovimentacoes() {
		return movimentacoes;
	}

	public void setMovimentacoes(List<Movimentacao> movimentacoes) {
		this.movimentacoes = movimentacoes;
	}
	
}
