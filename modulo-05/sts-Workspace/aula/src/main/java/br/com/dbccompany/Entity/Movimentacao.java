package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Movimentacao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	@ManyToMany(mappedBy = "movimentacoes")
	private List<Conta> contas = new ArrayList<Conta>();
	
	@Enumerated(EnumType.STRING)
	@Column(name = "TIPO_MOVIMENTACAO")
	private TipoMovimentacao tipoMovimentacao;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "MOVIMENTACAO_EMPRESTIMO",
			joinColumns = {
					@JoinColumn(name = "ID_MOVIMENTACAO")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_STATUS_EMPRESTIMO")})
	private List<StatusEmprestimo> statusEmprestimos = new ArrayList<StatusEmprestimo>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}

	public TipoMovimentacao getTipoMovimentacao() {
		return tipoMovimentacao;
	}

	public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
		this.tipoMovimentacao = tipoMovimentacao;
	}

	public List<StatusEmprestimo> getStatusEmprestimos() {
		return statusEmprestimos;
	}

	public void setStatusEmprestimos(List<StatusEmprestimo> statusEmprestimos) {
		this.statusEmprestimos = statusEmprestimos;
	}
	
	
}
