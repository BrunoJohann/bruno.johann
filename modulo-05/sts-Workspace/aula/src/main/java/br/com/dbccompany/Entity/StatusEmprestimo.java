package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class StatusEmprestimo {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private Integer parcelamento;
	
	@ManyToMany(mappedBy = "statusEmprestimos")
	private List<Movimentacao> movimentacoes = new ArrayList<Movimentacao>();
	
	@ManyToOne
	private EmprestimoAprovado emprestimoAprovado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParcelamento() {
		return parcelamento;
	}

	public void setParcelamento(Integer parcelamento) {
		this.parcelamento = parcelamento;
	}

	public List<Movimentacao> getMovimentacoes() {
		return movimentacoes;
	}

	public void setMovimentacoes(List<Movimentacao> movimentacoes) {
		this.movimentacoes = movimentacoes;
	}

	public EmprestimoAprovado getEmprestimoAprovado() {
		return emprestimoAprovado;
	}

	public void setEmprestimoAprovado(EmprestimoAprovado emprestimoAprovado) {
		this.emprestimoAprovado = emprestimoAprovado;
	}
	
	
}
