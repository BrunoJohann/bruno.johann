package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class EmprestimoAprovado {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private long valor;
	
	@OneToMany(mappedBy = "emprestimoAprovado")
	private List<StatusEmprestimo> statusEmprestimos = new ArrayList<StatusEmprestimo>();
	
	@ManyToOne
	private AprovacaoGerente aprovacaoGerente;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}

	public List<StatusEmprestimo> getStatusEmprestimos() {
		return statusEmprestimos;
	}

	public void setStatusEmprestimos(List<StatusEmprestimo> statusEmprestimos) {
		this.statusEmprestimos = statusEmprestimos;
	}
	
	
}
