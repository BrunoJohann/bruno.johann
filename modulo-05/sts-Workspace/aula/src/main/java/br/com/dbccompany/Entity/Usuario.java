package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
//import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String nome;
	
	private String senha;
	
	private long  CPF;
	
	private long RG;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "USUARIO_ENDERECO",
			joinColumns = {
					@JoinColumn(name = "ID_USUARIO")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_ENDERECO")})
	private List<Endereco> enderecos = new ArrayList<Endereco>();
	
	@ManyToOne
	private Banco banco;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_usuario")
	private TipoUsuario tipoUsuario;

	@OneToMany(mappedBy = "usuario")
	private List<Contato> contatos = new ArrayList<Contato>();
	
	@OneToMany(mappedBy = "usuario")
	private List<AprovacaoGerente> aprovacoesGerente = new ArrayList<AprovacaoGerente>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public long getCPF() {
		return CPF;
	}

	public void setCPF(long cPF) {
		CPF = cPF;
	}

	public long getRG() {
		return RG;
	}

	public void setRG(long rG) {
		RG = rG;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	
	
}
