import java.util.*;

public class ComparadorDeElfos implements Comparator<Elfo> {
    public int compare(Elfo elfoAtual, Elfo proximoElfo) {
        boolean mesmoTipo = elfoAtual.getClass() == proximoElfo.getClass();

        if (mesmoTipo) {
            return 0;
        }

        return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? -1 : 1;

    }
}