package br.com.dbc.bdigital.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.johann
 */
@Entity
@Table(name = "BANCO_TABELAO")
public class BancoTabelao {
    
    @Id
    @SequenceGenerator(allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
    @GeneratedValue(generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    //@Column(name = "NOME", length = 100, nullable = false)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    
}
