/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.bdigital.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author bruno.johann
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ",
        sequenceName = "CLIENTE_SEQ")
public class Cliente {
    
    @Id
    @GeneratedValue(generator = "CLIENTE_SEQ",
            strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    
}
