package br.com.dbc.bdigital;

import br.com.dbc.bdigital.entity.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.logging.annotations.Log;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {
    /**
     * @param args the command line arguments
     */   
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
    
    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            
            
            
            
            
            
            
            
            
            
            transaction.commit();
        }catch(Exception ex){
            if(transaction !=null)
                transaction.rollback();
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
        }finally{
            if(session != null)
                session.close();
            System.exit(0);
        }
    }
}
