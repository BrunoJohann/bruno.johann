/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.dao;


import br.com.dbc.jogos.entity.Agencia;

/**
 *
 * @author bruno.johann
 */
public class AgenciaDAO extends AbstractDAO<Agencia> {

    @Override
    protected Class<Agencia> getEntityClass() {
        return Agencia.class;
    }
    
    
}
