/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jogos.dao;

import br.com.dbc.jogos.dto.EnderecoDTO;
import br.com.dbc.jogos.entity.Cliente;
import br.com.dbc.jogos.entity.Endereco;
import br.com.dbc.jogos.entity.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bruno.johann
 */
public class EnderecoDAOTest {
    
    public EnderecoDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEntityClass method, of class EnderecoDAO.
     */
    @Test
    public void testGetEntityClass() {
        System.out.println("getEntityClass");
        EnderecoDAO instance = new EnderecoDAO();
        Class<Endereco> expResult = null;
        Class<Endereco> result = instance.getEntityClass();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of from method, of class EnderecoDAO.
     */
    @Test
    public void testFrom() {
        System.out.println("from");
        EnderecoDTO dto = null;
        Cliente c = null;
        EnderecoDAO instance = new EnderecoDAO();
        Endereco expResult = null;
        Endereco result = instance.from(dto, c);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
    @Test
    public void testeDeleteEnderecoDAO(){
        Endereco e = new Endereco();
        e.setLogradouro("Rua");
        e.setNumero(123);
        e.setBairro("bairro");
        e.setCidade("cidade");

        Session session = HibernateUtil.getSession(true);
        Cliente c = new Cliente();
        c.setNome("joao");
        c.setCpf("123");
        session.save(c);
        e.setCliente(c);
        session.save(e);
        HibernateUtil.commitTransaction();
        new EnderecoDAO().delete(e.getId());
        session = HibernateUtil.getSession(true);
        session.delete(c);
        HibernateUtil.commitTransaction();
        Assert.assertEquals(null, session.createCriteria(Endereco.class).add(Restrictions.eq("id", e.getId())).uniqueResult());

        
    }
    
    
    
}
