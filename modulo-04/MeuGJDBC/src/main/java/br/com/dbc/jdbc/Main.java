/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author guilherme.borges
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO'")
                    .executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE USUARIO(\n" +
                            "  ID NUMBER NOT NULL PRIMARY KEY,\n" +
                            "  NOME VARCHAR(100) NOT NULL,\n" +
                            "  APELIDO VARCHAR(15) NOT NULL,\n" +
                            "  SENHA VARCHAR(15) NOT NULL,\n" +
                            "  CPF NUMBER(11) NOT NULL\n" +
                            ")").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into banco(id, nome) " 
                            + "values (BANCO_SEQ.nextval, ?)");
            pst.setString(1, "Caixa");
//            pst.setString(2, "Dinho");
//            pst.setString(3, "159");
//            pst.setInt(4, 123);
            pst.executeUpdate();
            
            rs = conn.prepareStatement("select * from usuario").executeQuery();
            while(rs.next()) {
                System.out.println(String.format("id: %s", rs.getInt("id")));
                System.out.println(String.format("Nome: %s", rs.getString("nome")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName())
                    .log(Level.SEVERE, "Erro na consulta do Main", ex);
            System.out.println("executado");
        }
        
    }
    
}