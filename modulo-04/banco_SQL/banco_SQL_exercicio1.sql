CREATE TABLE USUARIO(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  APELIDO VARCHAR(15) NOT NULL,
  SENHA VARCHAR(15) NOT NULL,
  CPF NUMBER(11) NOT NULL
);

CREATE SEQUENCE USUARIO_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE CARTAO_CREDITO(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_USUARIO NUMBER NOT NULL REFERENCES USUARIO( ID ),
  BANDEIRA VARCHAR(50) NOT NULL,
  NUMERO NUMBER NOT NULL,
  NOME_ESCRITO VARCHAR(100) NOT NULL,
  CODIGO_SEGURANCA NUMBER NOT NULL,
  VALIDADE NUMBER NOT NULL
);

CREATE SEQUENCE CARTAO_CREDITO_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE TIPO_CONTATO(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(100) NOT NULL,
  QUANTIDADE NUMBER NOT NULL
);

CREATE SEQUENCE TIPO_CONTATO_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE CONTATO(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_USUARIO NUMBER NOT NULL REFERENCES USUARIO( ID ),
  ID_TIPO_CONTATO NUMBER NOT NULL REFERENCES TIPO_CONTATO( ID ),
  VALOR VARCHAR(100) NOT NULL
);

CREATE SEQUENCE CONTATO_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE ENDERECO(
  ID NUMBER NOT NULL PRIMARY KEY,
  LOGRADOURO VARCHAR(100) NOT NULL,
  NUMERO NUMBER NOT NULL,
  COMPLEMENTO VARCHAR(50) NOT NULL,
  BAIRRO VARCHAR(100) NOT NULL,
  CIDADE VARCHAR(100) NOT NULL
);

CREATE SEQUENCE ENDERECO_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE USUARIO_ENDERECO(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_USUARIO NUMBER NOT NULL REFERENCES USUARIO( ID ),
  ID_ENDERECO NUMBER NOT NULL REFERENCES ENDERECO( ID )
);

CREATE SEQUENCE USUARIO_ENDERECO_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE STATUS(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(50) NOT NULL
);

CREATE SEQUENCE STATUS_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE RACA(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(50) NOT NULL,
  LIMITADOR NUMBER DEFAULT 1,
  VIDA_INICIO NUMBER NOT NULL,
  DANO_INICIO NUMBER NOT NULL
);

CREATE SEQUENCE RACA_SEQ
  START WITH 1
  INCREMENT BY 1;

CREATE TABLE TIPO_RACA(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_RACA NUMBER NOT NULL REFERENCES RACA( ID ),
  NOME VARCHAR(50) NOT NULL,
  BONUS_VIDA NUMBER NOT NULL,
  BONUS_DANOS NUMBER NOT NULL
);

CREATE SEQUENCE TIPO_RACA_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE ITEM(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(50) NOT NULL,
  QTD_MAXIMA NUMBER NOT NULL
);

CREATE SEQUENCE ITEM_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE TIPO_RACA_ITEM(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_RACA NUMBER NOT NULL REFERENCES RACA( ID ),
  ID_ITEM NUMBER NOT NULL REFERENCES ITEM( ID )
);

CREATE SEQUENCE TIPO_RACA_ITEM_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE MATERIAIS(
  ID NUMBER NOT NULL PRIMARY KEY,
  NOME VARCHAR(50) NOT NULL,
  UNIDADE NUMBER NOT NULL
);

CREATE SEQUENCE MATERIAIS_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE MATERIAL_ITEM(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_ITEM NUMBER NOT NULL REFERENCES ITEM( ID ),
  ID_MATERIAIS NUMBER NOT NULL REFERENCES MATERIAIS( ID ),
  QUANTIDADE NUMBER NOT NULL
);

CREATE SEQUENCE MATERIAL_ITEM_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE INVENTARIO(
  ID NUMBER NOT NULL PRIMARY KEY,
  QTD_MAXIMA NUMBER NOT NULL
);

CREATE SEQUENCE INVENTARIO_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE LISTA_ITENS_INVENTARIO(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_INVENTARIO NUMBER NOT NULL REFERENCES INVENTARIO( ID ),
  ID_ITEM NUMBER NOT NULL REFERENCES ITEM( ID ),
  QUANTIDADE NUMBER NOT NULL
);

CREATE SEQUENCE LISTA_ITENS_INVENTARIO_SEQ
  START WITH 1
  INCREMENT BY 1;
  
CREATE TABLE PERSONAGEM(
  ID NUMBER NOT NULL PRIMARY KEY,
  ID_USUARIO NUMBER NOT NULL REFERENCES USUARIO( ID ),
  ID_RACA NUMBER NOT NULL REFERENCES RACA( ID ),
  ID_STATUS NUMBER NOT NULL REFERENCES STATUS( ID ),
  ID_INVENTARIO NUMBER NOT NULL REFERENCES INVENTARIO( ID ),
  NOME VARCHAR(100) NOT NULL,
  VIDA NUMBER NOT NULL,
  DANO NUMBER NOT NULL,
  XP NUMBER NOT NULL
);

CREATE SEQUENCE PERSONAGEM_SEQ
  START WITH 1
  INCREMENT BY 1;
  
INSERT INTO USUARIO( ID, NOME, APELIDO, SENHA, CPF ) VALUES( USUARIO_SEQ.NEXTVAL, 'Bruno', 'Dinho', 'senha1234', 12345678912 );
INSERT INTO USUARIO( ID, NOME, APELIDO, SENHA, CPF ) VALUES( USUARIO_SEQ.NEXTVAL, 'Gabriel', 'Gaba', '12senha34', 75642314597 );
INSERT INTO USUARIO( ID, NOME, APELIDO, SENHA, CPF ) VALUES( USUARIO_SEQ.NEXTVAL, 'Michele', 'Dada', 'se1234nha', 21987654321 );

INSERT INTO CARTAO_CREDITO( ID, ID_USUARIO, BANDEIRA, NUMERO, NOME_ESCRITO, CODIGO_SEGURANCA, VALIDADE ) VALUES( CARTAO_CREDITO_SEQ.NEXTVAL, 45, 'Master', 159, 'Bruno A.', 456, 0112);
INSERT INTO CARTAO_CREDITO( ID, ID_USUARIO, BANDEIRA, NUMERO, NOME_ESCRITO, CODIGO_SEGURANCA, VALIDADE ) VALUES( CARTAO_CREDITO_SEQ.NEXTVAL, 46, 'Visa', 45, 'Gabriel A.', 654, 0713);
INSERT INTO CARTAO_CREDITO( ID, ID_USUARIO, BANDEIRA, NUMERO, NOME_ESCRITO, CODIGO_SEGURANCA, VALIDADE ) VALUES( CARTAO_CREDITO_SEQ.NEXTVAL, 47, 'Master', 789, 'Michele D.', 258, 0218);

INSERT INTO TIPO_CONTATO( ID, NOME, QUANTIDADE) VALUES( TIPO_CONTATO_SEQ.NEXTVAL, 'Telefone', 33887777);
INSERT INTO TIPO_CONTATO( ID, NOME, QUANTIDADE) VALUES( TIPO_CONTATO_SEQ.NEXTVAL, 'Celular', 988887777);
INSERT INTO TIPO_CONTATO( ID, NOME, QUANTIDADE) VALUES( TIPO_CONTATO_SEQ.NEXTVAL, 'Whats', 88887777);

INSERT INTO CONTATO( ID, ID_USUARIO, ID_TIPO_CONTATO, VALOR) VALUES( CONTATO_SEQ.NEXTVAL, 45, 1, 'Telefone');
INSERT INTO CONTATO( ID, ID_USUARIO, ID_TIPO_CONTATO, VALOR) VALUES( CONTATO_SEQ.NEXTVAL, 46, 2, 'Celular');
INSERT INTO CONTATO( ID, ID_USUARIO, ID_TIPO_CONTATO, VALOR) VALUES( CONTATO_SEQ.NEXTVAL, 47, 3, 'Whats');

INSERT INTO ENDERECO( ID, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CIDADE) VALUES( ENDERECO_SEQ.NEXTVAL, 'Predio', 123, 1, 'Residencial', 'Eldorado do sul');
INSERT INTO ENDERECO( ID, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CIDADE) VALUES( ENDERECO_SEQ.NEXTVAL, 'Dirigivel', 145, 89, 'Xoxalon', 'Guaiba');
INSERT INTO ENDERECO( ID, LOGRADOURO, NUMERO, COMPLEMENTO, BAIRRO, CIDADE) VALUES( ENDERECO_SEQ.NEXTVAL, 'Casa', 368, 74, 'sei l�', 'Canoas');