package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.dto.EnderecoDTO;
import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.dto.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    /**
     * @param args the command line arguments
     */
    private static final Logger LOG = Logger.getLogger(Main.class.getName());
    
    public static void main(String[] args) {
        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        dto.setApelidoUsuario("Bruno");
        dto.setNomeUsuario("Bruno");
        dto.setCpfUsuario(123l);
        dto.setSenhaUsuario("123");
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setLogradouro("Rua do Bruno");
        enderecoDTO.setNumero(123);
        enderecoDTO.setBairro("Bairro do Bruno");
        enderecoDTO.setCidade("Cidade do Bruno");
        enderecoDTO.setComplemento("CEP 123");
        dto.setEndereco(enderecoDTO);
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Elfo do Bruno");
        personagemDTO.setDanoElfo(123d);
        dto.setPersonagem(personagemDTO);
        usuarioService.cadastrarUsuarioEPersonagem(dto);
        System.exit(0);
    }
    
    public static void oldMain(String[] args) {
        Session session = null;
        Transaction transaction = null;
        try{
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            
            Usuario usuario = new Usuario();
            usuario.setNome("billy");
            usuario.setApelido("malvado");
            usuario.setCpf(123L);
            usuario.setSenha("ugaugala");
            
            Endereco endereco = new Endereco();
            endereco.setBairro("Residêncial");
            endereco.setCidade("Eldorado");
            endereco.setComplemento("Nada");
            endereco.setLogradouro("Casa");
            endereco.setNumero(100);
            
            Endereco endereco2 = new Endereco();
            endereco2.setBairro("Residêncial");
            endereco2.setCidade("Eldorado");
            endereco2.setComplemento("Nada");
            endereco2.setLogradouro("Casa");
            endereco2.setNumero(150);
            
            TipoContato tipoContato = new TipoContato();
            tipoContato.setName("Celular");
            tipoContato.setQuantidade(12);
            
            Contato contato = new Contato();
            contato.setTipoContato(tipoContato);
            contato.setUsuario(usuario);
            contato.setValor("88884444");

            tipoContato.setContato(contato);
            usuario.pushContatos(contato);
            usuario.pushEnderecos(endereco, endereco2);
            
            session.save(usuario);

            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d);
            elfoTabelao.setNome("Legolas");
            session.save(elfoTabelao);
            
            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(10d);
            hobbitTabelao.setNome("Frodo");
            session.save(hobbitTabelao);
            
            ElfoPerClass elfoPerClass = new ElfoPerClass();
            elfoPerClass.setNome("Legolas Per Class");
            elfoPerClass.setDanoElfo(100d);
            session.save(elfoPerClass);
            
            HobbitPerClass hobbitPerClass = new HobbitPerClass();
            hobbitPerClass.setNome("Bilbo");
            hobbitPerClass.setDanoHobbit(1d);
            session.save(hobbitPerClass);
            
            ElfoJoin elfoJoin = new ElfoJoin();
            elfoJoin.setNome("Legolas Join");
            elfoJoin.setDanoElfo(100d);
            session.save(elfoJoin);
            
            HobbitJoin hobbitJoin = new HobbitJoin();
            hobbitJoin.setNome("Sam");
            hobbitJoin.setDanoHobbit(1d);
            session.save(hobbitJoin);
            
            Criteria criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions.
                    and(
                            Restrictions.ilike("endereco.bairro", "%resid%"),
                            Restrictions.ilike("endereco.cidade", "%eldora%")
                    ));
            
            List<Usuario> usuarios = criteria.list();
            
            usuarios.forEach(System.out::println);
            
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions.
                    and(
                            Restrictions.ilike("endereco.bairro", "%resid%"),
                            Restrictions.ilike("endereco.cidade", "%eldora%")
                    ));
            
            criteria.setProjection(Projections.rowCount());
            
            criteria.setProjection(Projections.count("id"));
            
            
            System.out.println(String
                    .format("Foram encontrados %s registro(s) " 
                            + "com os critérios", criteria.uniqueResult()));
           
            
            
            session.createQuery("select u from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%eldora%' "
                    + " and lower(endereco.bairro) like '%resid%' ");
            
            usuarios.forEach(System.out::println);
            
            Long count = (Long)session
                    .createQuery("select count(distinct u.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%eldora%' "
                    + " and lower(endereco.bairro) like '%resid%' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s valores", count));
            
            count = (Long)session
                    .createQuery("select count(endereco.id) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%eldora%' "
                    + " and lower(endereco.bairro) like '%resid%' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s enderecos", count));
            
            Long sum = (Long)session
                    .createQuery("select count(endereco.numero) from Usuario u "
                    + " join u.enderecos endereco "
                    + " where lower(endereco.cidade) like '%eldora%' "
                    + " and lower(endereco.bairro) like '%resid%' ")
                    .uniqueResult();
            System.out.println(String.format("Contamos com HQL %s numeros", sum));
            
            
            transaction.commit();
        }catch(Exception ex){
            if(transaction !=null)
                transaction.rollback();
            LOG.log(Level.SEVERE, ex.getMessage(), ex);
            System.exit(1);
        }finally{
            if(session != null)
                session.close();
            System.exit(0);
        }
    }
}
