/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.service;

import br.com.dbc.lotr.dto.PersonagemDTO;
import br.com.dbc.lotr.HibernateUtil;
import br.com.dbc.lotr.dao.ElfoDAO;
import br.com.dbc.lotr.dao.HobbitDAO;
import br.com.dbc.lotr.entity.Usuario;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Transaction;

/**
 *
 * @author bruno.johann
 */
public class PersonagemService {
    
    private static final ElfoDAO ELFO_DAO = new ElfoDAO();
    private static final HobbitDAO HOBBIT_DAO = new HobbitDAO();
    
    public void salvarPersonagem(PersonagemDTO personagemDTO, Usuario usuario){
        boolean started = HibernateUtil.beginTransaction();
        Transaction transaction = HibernateUtil.getSession().getTransaction();
        try{
            
            switch(personagemDTO.getRaca()){
                case ELFO:
                    ELFO_DAO.criar(ELFO_DAO.parseFrom(personagemDTO, usuario));
                    break;
                case HOBBIT:
                    HOBBIT_DAO.criar(HOBBIT_DAO.parseFrom(personagemDTO, usuario));
                    break;
            }
            if(started){
                transaction.commit();
            }
        } catch (Exception e){
            transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }   
    }
    private static final Logger LOG = Logger
            .getLogger(PersonagemService.class.getName());
     
}
