/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.johann
 */
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
*
* @author antonio.vidal
*/
@Entity
@Table(name = "Crediario")
@SequenceGenerator(allocationSize = 1, name = "Crediario_seq", sequenceName = "Crediario_seq")
public class Crediario {
   @Id
   @GeneratedValue(generator = "Crediario_seq", strategy = GenerationType.SEQUENCE)
   private Integer id;
   
   @OneToOne(mappedBy = "crediario")
   @JoinColumn(name = "Id_Cliente")
   private Cliente3 cliente;
}
