
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.johann
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "PIKACHU_SEQ",
        sequenceName = "PIKACHU_SEQ")
public class Pikachu {
    
    @Id
    @GeneratedValue(generator = "PIKACHU_SEQ",
            strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToMany(mappedBy = "pikachu")
    private List<Evolucao> evolucao = new ArrayList();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Evolucao> getEvolucao() {
        return evolucao;
    }

    public void setEvolucao(List<Evolucao> evolucao) {
        this.evolucao = evolucao;
    }
    
    
    
    
}
