/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.johann
 */
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

/**
*
* @author antonio.vidal
*/
@Entity
@SequenceGenerator(allocationSize = 1, name = "Cliente3_seq", sequenceName = "Cliente3_seq")
public class Cliente3 {
   @Id
   @GeneratedValue(generator = "Cliente3_seq", strategy = GenerationType.SEQUENCE)
   private Integer id;
   
   @OneToOne()
   @JoinColumn(name = "Id_Crediario")
   private Crediario crediario;
}