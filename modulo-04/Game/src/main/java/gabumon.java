import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.johann
 */
@Entity
@Table(name = "gabumon")
@DiscriminatorValue("GABUMON")
public class gabumon extends Digimon{
    
    @Column(name = "forca_gelo")
    private Integer forcaGelo;
    
    @Column(name = "nome")
    private String nome;
    
}