
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.johann
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "ATAQUE_ESPECIAL_SEQ",
        sequenceName = "ATAQUE_ESPECIAL_SEQ")
public class AtaqueEspecial {
    @Id
    @GeneratedValue(generator = "ATAQUE_ESPECIAL_SEQ",
            strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne()
    @JoinColumn(name = "id_charmeleon")
    private Charmeleon charmeleon;
    
}
