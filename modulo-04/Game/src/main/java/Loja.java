
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.johann
 */
@Entity
@Table(name = "Loja")
@SequenceGenerator(allocationSize = 1, name = "loja_seq", sequenceName = "loja_seq")
public class Loja {
    
    @Id
    @GeneratedValue(generator = "loja_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "cliente_loja",
            joinColumns = { 
                @JoinColumn(name = "id_Loja")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_Cliente")})
    private List<Cliente2> clientes;
}
