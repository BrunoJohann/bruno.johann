
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bruno.johann
 */
@Entity
@SequenceGenerator(allocationSize = 1, name = "EVOLUCAO_SEQ",
        sequenceName = "EVOLUCAO_SEQ")
public class Evolucao {
    
    @Id
    @GeneratedValue(generator = "EVOLUCAO_SEQ",
            strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne()
    @JoinColumn(name = "id_pikachu")
    private Pikachu pikachu;
    
}
