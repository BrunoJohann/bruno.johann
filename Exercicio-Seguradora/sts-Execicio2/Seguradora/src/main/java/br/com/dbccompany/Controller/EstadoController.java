package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Service.EstadoService;
import br.com.dbccompany.Service.ServicoContratadoService;

@Controller
@RequestMapping("api/estado")
public class EstadoController {

	@Autowired
	private EstadoService estadoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Estado> lstEstados() {
		return estadoService.allEstados();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Estado novoEstado(@RequestBody Estado estado) {
		return estadoService.salvarEstado(estado);
	}
	
	@GetMapping(value = "/{nome}")
	@ResponseBody
	public Estado buscarServicosPorNome(@PathVariable String nome) {
		return estadoService.buscarPorNome(nome);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Estado editarEstado(@PathVariable Integer id,@RequestBody Estado estado) {
		return estadoService.editarEstado(id, estado);
	}
	
}
