package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Repository.SeguradosRepository;

@Service
public class SeguradosService {

	@Autowired
	private SeguradosRepository seguradosRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Segurados salvarSegurados(Segurados segurados) {
		return seguradosRepository.save(segurados);
	}
	
	public Optional<Segurados> buscarSegurados(Integer id) {
		return seguradosRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Segurados editarSegurados(Integer id, Segurados segurados) {
		segurados.setId(id);
		return seguradosRepository.save(segurados);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarServicos(Integer id) {
		seguradosRepository.deleteById(id);
	}
	
	public List<Segurados> allSegurados() {
		return (List<Segurados>) seguradosRepository.findAll();
	}
	
	public Segurados buscarPorNome(String nome) {
		return seguradosRepository.findByNome(nome);
	}
	
}
