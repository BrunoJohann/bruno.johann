package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.SeguradoraRepository;

@Service
public class SeguradoraService {

	@Autowired
	private SeguradoraRepository seguradoraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora salvarSeguradora(Seguradora seguradora) {
		return seguradoraRepository.save(seguradora);
	}
	
	public Optional<Seguradora> buscarSeguradora(Integer id) {
		return seguradoraRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Seguradora editarSeguradora(Integer id, Seguradora seguradora) {
		seguradora.setId(id);
		return seguradoraRepository.save(seguradora);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarSeguradora(Integer id) {
		seguradoraRepository.deleteById(id);
	}
	
	public List<Seguradora> allSeguradoras() {
		return (List<Seguradora>) seguradoraRepository.findAll();
	}
	
	public Seguradora buscarPorNome(String nome) {
		return seguradoraRepository.findByNome(nome);
	}
	
}
