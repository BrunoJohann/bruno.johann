package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Cidade {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ")
	@GeneratedValue(generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String nome;
	
	@ManyToOne
	@JoinColumn(name = "ID_ESTADO")
	private Estado estado;
	
	@OneToMany(mappedBy = "cidade")
	private List<Bairro> bairro = new ArrayList<Bairro>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Bairro> getBairro() {
		return bairro;
	}

	public void setBairro(List<Bairro> bairro) {
		this.bairro = bairro;
	}

}
