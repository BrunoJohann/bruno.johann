package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Service.CorretorService;

@Controller
@RequestMapping("api/corretor")
public class CorretorController {

	@Autowired
	private CorretorService corretorService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Corretor> lstCorretor() {
		return corretorService.allCorretores();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Corretor novoCorretor(@RequestBody Corretor corretor) {
		return corretorService.salvarCorretor(corretor);
	}
	
	@GetMapping(value = "/{nome}")
	@ResponseBody
	public Corretor buscarPorSeguradora(@PathVariable String nome) {
		return corretorService.buscarPorNome(nome);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Corretor editarCorretor(@PathVariable Integer id,@RequestBody Corretor Corretor) {
		return corretorService.editarCorretor(id, Corretor);
	}
}
