package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Repository.CorretorRepository;

@Service
public class CorretorService {

	@Autowired
	private CorretorRepository corretorRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor salvarCorretor(Corretor corretor) {
		return corretorRepository.save(corretor);
	}
	
	public Optional<Corretor> buscarCorretor(Integer id) {
		return corretorRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Corretor editarCorretor(Integer id, Corretor corretor) {
		corretor.setId(id);
		return corretorRepository.save(corretor);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarCorretor(Integer id) {
		corretorRepository.deleteById(id);
	}
	
	public List<Corretor> allCorretores() {
		return (List<Corretor>) corretorRepository.findAll();
	}
	
	public Corretor buscarPorNome(String nome) {
		return corretorRepository.findByNome(nome);
	}
	
}
