package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Entity.ServicoContratado;

public interface SeguradosRepository extends PessoasRepository<Segurados>{
	
	Segurados findByQtdServicos(long qtdServicos);
	Segurados findByServicosContratados(List<ServicoContratado> servicosContratados);
	Segurados findByServicosContratados(ServicoContratado servicosContratados);
	
}
