package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Service.BairroService;
import br.com.dbccompany.Service.EnderecoSeguradorasService;

@Controller
@RequestMapping("api/bairro")
public class BairroController {

	@Autowired
	private BairroService bairroService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Bairro> lstBairros() {
		return bairroService.allBairros();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Bairro novoBairro(@RequestBody Bairro bairro) {
		return bairroService.salvarBairro(bairro);
	}
	
	@GetMapping(value = "/{nome}")
	@ResponseBody
	public Bairro buscarPorNomeDoBairro(@PathVariable String nome) {
		return bairroService.buscarPorNomeDoBairro(nome);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Bairro editarBairro(@PathVariable Integer id,@RequestBody Bairro bairro) {
		return bairroService.editarBairro(id, bairro);
	}
	
}
