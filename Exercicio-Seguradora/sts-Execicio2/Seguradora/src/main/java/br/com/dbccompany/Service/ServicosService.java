package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Servicos;
import br.com.dbccompany.Repository.SeguradoraRepository;
import br.com.dbccompany.Repository.ServicoContratadoRepository;
import br.com.dbccompany.Repository.ServicosRepository;

@Service
public class ServicosService {

	@Autowired
	private ServicosRepository servicosRepository;
	
	@Autowired
	private SeguradoraRepository seguradoraRepository;
	
	@Autowired
	private ServicoContratadoRepository servicoContratadoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos salvarServicos(Servicos servicos) {
		return servicosRepository.save(servicos);
	}
	
	public Optional<Servicos> buscarServicos(Integer id) {
		return servicosRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Servicos editarServicos(Integer id, Servicos servicos) {
		servicos.setId(id);
		return servicosRepository.save(servicos);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarServicos(Integer id) {
		servicosRepository.deleteById(id);
	}
	
	public List<Servicos> allServicos() {
		return (List<Servicos>)servicosRepository.findAll();
	}
	
	public Servicos buscarPorNome(String nome) {
		return servicosRepository.findByNome(nome);
	}
	
	public Servicos buscarPorDescricao(String descricao) {
		return servicosRepository.findByDescricao(descricao);
	}
	
	public Servicos buscarPorValorPadrao(long valorPadrao) {
		return servicosRepository.findByValorPadrao(valorPadrao);
	}
	
	public List<Servicos> buscarPorNomeDaSeguradora(String nome) {
		return (List<Servicos>) servicosRepository.findBySeguradoras(seguradoraRepository.findByNome(nome));
	}
	
	public Servicos buscarPorServicoContratado(String descricao) {
		return servicosRepository.findByServicosContratados(servicoContratadoRepository.findByDescricao(descricao));
	}
	
}
