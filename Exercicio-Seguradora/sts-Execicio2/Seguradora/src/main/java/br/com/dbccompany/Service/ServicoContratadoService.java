package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.CorretorRepository;
import br.com.dbccompany.Repository.SeguradosRepository;
import br.com.dbccompany.Repository.ServicoContratadoRepository;

@Service
public class ServicoContratadoService {

	@Autowired
	private ServicoContratadoRepository servicoContratadoRepository;
	
	@Autowired
	private CorretorRepository corretorRepository;
	
	@Autowired
	private SeguradosRepository seguradosRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado salvarServicoContratado(ServicoContratado servicoContratado) {
		Corretor corretor = corretorRepository.findById(servicoContratado.getCorretor().getId()).get();
		Segurados segurados = seguradosRepository.findById(servicoContratado.getSegurados().getId()).get();
		segurados.setQtdServicos(segurados.getQtdServicos() + 1);
		corretor.setComissao(corretor.getComissao() + (servicoContratado.getValor() * 0.1));
		servicoContratado.setCorretor(corretor);
		servicoContratado.setSegurados(segurados);
		return servicoContratadoRepository.save(servicoContratado);
	}
	
	public Optional<ServicoContratado> buscarServicoContratado(Integer id) {
		return servicoContratadoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public ServicoContratado editarServicoContratado(Integer id, ServicoContratado servicoContratado) {
		servicoContratado.setId(id);
		return servicoContratadoRepository.save(servicoContratado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarServicoContratado(Integer id) {
		servicoContratadoRepository.deleteById(id);
	}
	
	public List<ServicoContratado> allServicoContratado() {
		return (List<ServicoContratado>) servicoContratadoRepository.findAll();
	}
	
	public ServicoContratado buscarPorDescricao(String descricao) {
		return servicoContratadoRepository.findByDescricao(descricao);
	}
	
}
