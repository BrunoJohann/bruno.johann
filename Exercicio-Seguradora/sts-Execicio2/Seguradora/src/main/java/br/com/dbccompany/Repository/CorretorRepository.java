package br.com.dbccompany.Repository;

import java.util.List;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.ServicoContratado;

public interface CorretorRepository extends PessoasRepository<Corretor>{

	Corretor findByCargo(String cargo);
	Corretor findByComissao(long comissao);
	List<Corretor> findByServicosContratados(ServicoContratado... servicosContratados);
	List<Corretor> findByServicosContratados(ServicoContratado servicosContratados);
	
}
