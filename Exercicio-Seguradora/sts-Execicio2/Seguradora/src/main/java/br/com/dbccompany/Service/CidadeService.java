package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Repository.CidadeRepository;

@Service
public class CidadeService {

	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade salvarCidade(Cidade cidade) {
		return cidadeRepository.save(cidade);
	}
	
	public Optional<Cidade> buscarCidade(Integer id) {
		return cidadeRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Cidade editarCidade(Integer id, Cidade cidade) {
		cidade.setId(id);
		return cidadeRepository.save(cidade);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarCidade(Integer id) {
		cidadeRepository.deleteById(id);
	}
	
	public List<Cidade> allCidades() {
		return (List<Cidade>) cidadeRepository.findAll();
	}
	
	public Cidade buscarPorNomeDaCidade(String nome) {
		return cidadeRepository.findByNome(nome);
	}
}
