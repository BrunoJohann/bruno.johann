package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;

public interface EnderecoSeguradorasRepository extends EnderecoRepository<EnderecoSeguradoras>{
	
	EnderecoSeguradoras findBySeguradora(Seguradora seguradora);

}
