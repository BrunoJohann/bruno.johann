package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Service.EnderecoService;

@Controller
@RequestMapping("api/endereco")
public class EnderecoController {

	@Autowired
	private EnderecoService enderecoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Endereco> lstEndereco() {
		return enderecoService.allEnderecos();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Endereco novoEndereco(@RequestBody Endereco endereco) {
		return enderecoService.salvarEndereco(endereco);
	}
	
	@GetMapping(value = "/{numero}")
	@ResponseBody
	public Endereco buscarPorNumero(@PathVariable long numero) {
		return enderecoService.buscarPorNumero(numero);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Endereco editarEndereco(@PathVariable Integer id,@RequestBody Endereco endereco) {
		return enderecoService.editarEndereco(id, endereco);
	}
	
	
}
