package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Service.CidadeService;

@Controller
@RequestMapping("api/cidade")
public class CidadeController {

	@Autowired
	private CidadeService cidadeService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Cidade> lstCidades() {
		return cidadeService.allCidades();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Cidade novaCidade(@RequestBody Cidade cidade) {
		return cidadeService.salvarCidade(cidade);
	}
	
	@GetMapping(value = "/{nome}")
	@ResponseBody
	public Cidade buscarPorNomeDaCidade(@PathVariable String nome) {
		return cidadeService.buscarPorNomeDaCidade(nome);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Cidade editarCidade(@PathVariable Integer id,@RequestBody Cidade cidade) {
		return cidadeService.editarCidade(id, cidade);
	}
}
