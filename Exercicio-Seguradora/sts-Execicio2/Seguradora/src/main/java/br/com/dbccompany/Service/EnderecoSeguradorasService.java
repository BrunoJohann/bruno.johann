package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Repository.EnderecoSeguradorasRepository;
import br.com.dbccompany.Repository.SeguradoraRepository;

@Service
public class EnderecoSeguradorasService {

	@Autowired
	private EnderecoSeguradorasRepository enderecoSeguradorasRepository;
	
	@Autowired
	private SeguradoraRepository seguradoraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradoras salvarEnderecoSeguradoras(EnderecoSeguradoras enderecoSeguradoras) {
		return enderecoSeguradorasRepository.save(enderecoSeguradoras);
	}
	
	public Optional<EnderecoSeguradoras> buscarEnderecoSeguradoras(Integer id) {
		return enderecoSeguradorasRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public EnderecoSeguradoras editarEnderecoSeguradoras(Integer id, EnderecoSeguradoras enderecoSeguradoras) {
		enderecoSeguradoras.setId(id);
		return enderecoSeguradorasRepository.save(enderecoSeguradoras);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarEnderecoSeguradora(Integer id) {
		enderecoSeguradorasRepository.deleteById(id);
	}
	
	public List<EnderecoSeguradoras> allEnderecoSeguradoras() {
		return (List<EnderecoSeguradoras>) enderecoSeguradorasRepository.findAll();
	}
	
	public EnderecoSeguradoras buscarPorNomeDaSeguradora(String nome) {
		return enderecoSeguradorasRepository.findBySeguradora(seguradoraRepository.findByNome(nome));
	}
	
}
