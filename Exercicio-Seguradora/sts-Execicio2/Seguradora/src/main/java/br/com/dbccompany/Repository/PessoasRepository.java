package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Pessoas;

public interface PessoasRepository<E extends Pessoas> extends CrudRepository<E, Integer>{

	E findByNome(String nome);
	E findByCpf(long cpf);
	E findByPai(String pai);
	E findByMae(String mae);
	E findByTelefone(long telefone);
	E findByEmail(String email);
//  E findByEnderecos(List<Endereco> enderecos);
	List<E> findByEnderecos(Endereco enderecos);
	
	
}
