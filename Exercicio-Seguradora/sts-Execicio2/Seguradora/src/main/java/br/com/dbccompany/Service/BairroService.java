package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Repository.BairroRepository;

@Service
public class BairroService {

	@Autowired
	private BairroRepository bairroRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro salvarBairro(Bairro bairro) {
		return bairroRepository.save(bairro);
	}
	
	public Optional<Bairro> buscarBairro(Integer id) {
		return bairroRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Bairro editarBairro(Integer id, Bairro bairro) {
		bairro.setId(id);
		return bairroRepository.save(bairro);
	}

	@Transactional(rollbackFor = Exception.class)
	public void deletarBairro(Integer id) {
		bairroRepository.deleteById(id);
	}
	
	public List<Bairro> allBairros() {
		return (List<Bairro>) bairroRepository.findAll();
	}
	
	public Bairro buscarPorNomeDoBairro(String nome) {
		return bairroRepository.findByNome(nome);
	}
	
	
}
