package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Service.SeguradoraService;

@Controller
@RequestMapping("api/seguradora")
public class SeguradoraController {

	@Autowired
	private SeguradoraService seguradoraService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Seguradora> lstSeguradora() {
		return seguradoraService.allSeguradoras();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Seguradora novaSeguradora(@RequestBody Seguradora seguradora) {
		return seguradoraService.salvarSeguradora(seguradora);
	}
	
	@GetMapping(value = "/{nome}")
	@ResponseBody
	public Seguradora buscarSeguradoraPorNome(@PathVariable String nome) {
		return seguradoraService.buscarPorNome(nome);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Seguradora editarSeguradora(@PathVariable Integer id,@RequestBody Seguradora seguradora) {
		return seguradoraService.editarSeguradora(id, seguradora);
	}
	
}
