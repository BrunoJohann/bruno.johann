package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Estado;

public interface CidadeRepository extends CrudRepository<Cidade, Integer>{

	Cidade findByNome(String nome);
	Cidade findByEstado(Estado estado);
	Cidade findByBairro(List<Bairro> bairro);
	Cidade findByBairro(Bairro bairro);
	
}
