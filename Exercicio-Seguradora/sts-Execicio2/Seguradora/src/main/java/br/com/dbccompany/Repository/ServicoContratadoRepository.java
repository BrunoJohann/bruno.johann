package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Entity.Servicos;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Integer>{

	ServicoContratado findByDescricao(String descricao);
	ServicoContratado findByValor(long valor);
	ServicoContratado findByServicos(Servicos servicos);
	ServicoContratado findBySeguradora(Seguradora seguradora);
	ServicoContratado findBySegurados(Segurados segurados);
	ServicoContratado findByCorretor(Corretor corretor);
	
}
