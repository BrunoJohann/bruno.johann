package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.ServicoContratado;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Integer>{

	Seguradora findByNome(String nome);
	Seguradora findByCnpj(long cnpj);
	Seguradora findByServicosContratados(List<ServicoContratado> servicosContratados);
	Seguradora findByServicosContratados(ServicoContratado servicosContratados);
	
	Seguradora findByEnderecoSeguradora(List<EnderecoSeguradoras> enderecoSeguradora);
	Seguradora findByEnderecoSeguradora(EnderecoSeguradoras enderecoSeguradora);
	
	
	
	
}
