package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Seguradora {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "SEGURADORA_SEQ", sequenceName = "SEGURADORA_SEQ")
	@GeneratedValue(generator = "SEGURADORA_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String nome; 
	
	private long cnpj;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "SEGURADORA_SERVICOS",
			joinColumns = {
					@JoinColumn(name = "ID_SEGURADORA")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_SERVICOS")})
	private List<Servicos> servicos = new ArrayList<Servicos>();
	
	@OneToMany(mappedBy = "seguradora")
	private List<ServicoContratado> servicosContratados = new ArrayList<ServicoContratado>();
	
	@OneToOne(mappedBy = "seguradora")
	@JoinColumn(name = "ID_ENDERECOS_ENDERECO_SEGURADORAS")
	private EnderecoSeguradoras enderecoSeguradora;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}

	public List<Servicos> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servicos> servicos) {
		this.servicos = servicos;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void setServicosContratados(List<ServicoContratado> servicosContratados) {
		this.servicosContratados = servicosContratados;
	}

	public EnderecoSeguradoras getEnderecoSeguradora() {
		return enderecoSeguradora;
	}

	public void setEnderecoSeguradora(EnderecoSeguradoras enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}
	
}
