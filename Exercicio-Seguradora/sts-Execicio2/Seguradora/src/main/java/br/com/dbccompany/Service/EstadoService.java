package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;


@Service
public class EstadoService {

	@Autowired
	private EstadoRepository estadoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Estado salvarEstado(Estado estado) {
		return estadoRepository.save(estado);
	}
	
	public Optional<Estado> buscarEstado(Integer id) {
		return estadoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Estado editarEstado(Integer id, Estado estado) {
		estado.setId(id);
		return estadoRepository.save(estado);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarEstado(Integer id) {
		estadoRepository.deleteById(id);
	}
	
	public List<Estado> allEstados() {
		return (List<Estado>) estadoRepository.findAll();
	}
	
	public Estado buscarPorNome(String nome) {
		return estadoRepository.findByNome(nome);
	}
	
}
