package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Segurados;
import br.com.dbccompany.Service.SeguradosService;

@Controller
@RequestMapping("api/segurados")
public class SeguradosController {

	@Autowired
	private SeguradosService seguradosService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<Segurados> lstSegurados() {
		return seguradosService.allSegurados();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public Segurados novoSegurado(@RequestBody Segurados segurado) {
		return seguradosService.salvarSegurados(segurado);
	}
	
	@GetMapping(value = "/{nome}")
	@ResponseBody
	public Segurados buscarSeguradosPorNome(@PathVariable String nome) {
		return seguradosService.buscarPorNome(nome);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public Segurados editarSegurado(@PathVariable Integer id,@RequestBody Segurados segurado) {
		return seguradosService.editarSegurados(id, segurado);
	}
	
}
