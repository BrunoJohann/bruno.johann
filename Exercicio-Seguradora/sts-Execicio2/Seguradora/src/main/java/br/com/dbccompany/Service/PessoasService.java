package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Pessoas;
import br.com.dbccompany.Repository.PessoasRepository;

@Service
public class PessoasService {

	@Autowired
	PessoasRepository<Pessoas> pessoasRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas salvarPessoas(Pessoas pessoas) {
		return pessoasRepository.save(pessoas);
	}
	
	public Optional<Pessoas> buscarPessoas(Integer id) {
		return pessoasRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Pessoas editarPessoas(Integer id, Pessoas pessoas) {
		pessoas.setId(id);
		return pessoasRepository.save(pessoas);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarPessoas(Integer id) {
		pessoasRepository.deleteById(id);
	}
	
	public List<Pessoas> allPessoas() {
		return (List<Pessoas>) pessoasRepository.findAll();
	}
	
	public Pessoas buscarPorNome(String nome) {
		return pessoasRepository.findByNome(nome);
	}
	
	public Pessoas buscarPorCPF(long cpf) {
		return pessoasRepository.findByCpf(cpf);
	}
	
	public Pessoas buscarPorNomeDoPai(String nomeDoPai) {
		return pessoasRepository.findByPai(nomeDoPai);
	}
	
	public Pessoas buscarPorNomeDaMae(String nomeDaMae) {
		return pessoasRepository.findByMae(nomeDaMae);
	}
	
	public Pessoas buscarPorTelefone(long telefone) {
		return pessoasRepository.findByTelefone(telefone);
	}
	
	public Pessoas buscarPorEmail(String email) {
		return pessoasRepository.findByEmail(email);
	}
	
	public List<Pessoas> buscarPorEndereco(Endereco enderecos) {
		return pessoasRepository.findByEnderecos(enderecos);
	}
	
}
