package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Estado;

public interface EstadoRepository extends CrudRepository<Estado, Integer>{

	Estado findByNome(String nome);
	Estado findByCidades(List<Cidade> cidade);
	Estado findByCidades(Cidade cidades);
	List<Estado> findAll();
	
	
}
