package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Service.EnderecoSeguradorasService;

@Controller
@RequestMapping("api/endereco_seguradora")
public class EnderecoSeguradorasController {

	@Autowired
	private EnderecoSeguradorasService enderecoSeguradorasService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<EnderecoSeguradoras> lstEnderecoSeguradoras() {
		return enderecoSeguradorasService.allEnderecoSeguradoras();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public EnderecoSeguradoras novoEnderecoSeguradoras(@RequestBody EnderecoSeguradoras enderecoSeguradoras) {
		return enderecoSeguradorasService.salvarEnderecoSeguradoras(enderecoSeguradoras);
	}
	
	@GetMapping(value = "/{nome}")
	@ResponseBody
	public EnderecoSeguradoras buscarPorSeguradora(@PathVariable String nome) {
		return enderecoSeguradorasService.buscarPorNomeDaSeguradora(nome);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public EnderecoSeguradoras editarEnderecoSeguradora(@PathVariable Integer id,@RequestBody EnderecoSeguradoras enderecoSeguradoras) {
		return enderecoSeguradorasService.editarEnderecoSeguradoras(id, enderecoSeguradoras);
	}
	
	
}
