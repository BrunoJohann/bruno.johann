package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Pessoas;

public interface EnderecoRepository<E extends Endereco> extends CrudRepository<E, Integer>{

	E findByLogradouro(String logradouro);
	E findByNumero(long numero);
	E findByComplemento(String complemento);
	E findByBairro(Bairro bairro);
//	Endereco findByPessoas(List<Pessoas> pessoas);
	List<E> findByPessoas(List<Pessoas> pessoas);
	E findByEnderecoSeguradora(List<EnderecoSeguradoras> enderecoSeguradora);
	E findByEnderecoSeguradora(EnderecoSeguradoras enderecoSeguradora);
	
}
