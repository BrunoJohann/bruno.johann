package br.com.dbccompany.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class ServicoContratado {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "SERVICO_CONTRATADO_SEQ", sequenceName = "SERVICO_CONTRATADO_SEQ")
	@GeneratedValue(generator = "SERVICO_CONTRATADO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String descricao;
	
	private long valor;
	
	@ManyToOne
	@JoinColumn(name = "ID_SERVICOS")
	private Servicos servicos;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_SEGURADORA")
	private Seguradora seguradora;
	
	@ManyToOne
	@JoinColumn(name = "ID_PESSOAS_SEGURADOS")
	private Segurados segurados;
	
	@ManyToOne
	@JoinColumn(name = "ID_PESSOAS_CORRETOR")
	private Corretor corretor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}

	public Servicos getServicos() {
		return servicos;
	}

	public void setServicos(Servicos servicos) {
		this.servicos = servicos;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

	public Segurados getSegurados() {
		return segurados;
	}

	public void setSegurados(Segurados segurados) {
		this.segurados = segurados;
	}

	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}

}
