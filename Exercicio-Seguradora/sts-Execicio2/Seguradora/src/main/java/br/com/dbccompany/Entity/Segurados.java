package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "SEGURADOS")
@PrimaryKeyJoinColumn(name = "ID_PESSOAS")
public class Segurados extends Pessoas{
	
	@JoinColumn(name = "QTD_SERVICOS")
	private long qtdServicos;
	
	@OneToMany(mappedBy = "segurados")
	//@JoinColumn(name = "ID_SERVICOS_CONTRATADOS")
	private List<ServicoContratado> servicosContratados = new ArrayList<ServicoContratado>();

	public long getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(long qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void setServicosContratados(List<ServicoContratado> servicosContratados) {
		this.servicosContratados = servicosContratados;
	}

}
