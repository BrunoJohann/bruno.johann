package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Service.ServicoContratadoService;

@Controller
@RequestMapping("api/servico_contratado")
public class ServicoContratadoController {

	@Autowired
	private ServicoContratadoService servicoContratadoService;
	
	@GetMapping(value = "/")
	@ResponseBody
	public List<ServicoContratado> lstServicos() {
		return servicoContratadoService.allServicoContratado();
	}
	
	@PostMapping(value = "/novo")
	@ResponseBody
	public ServicoContratado novoServicoContratadoService(@RequestBody ServicoContratado servicoContratado) {
		return servicoContratadoService.salvarServicoContratado(servicoContratado);
	}
	
	@GetMapping(value = "/{nome}")
	@ResponseBody
	public ServicoContratado buscarServicosPorDescricao(@PathVariable String descricao) {
		return servicoContratadoService.buscarPorDescricao(descricao);
	}
	
	@PutMapping(value = "/editar/{id}")
	@ResponseBody
	public ServicoContratado editarServicoContratado(@PathVariable Integer id,@RequestBody ServicoContratado servicoContratado) {
		return servicoContratadoService.editarServicoContratado(id, servicoContratado);
	}
	
}
