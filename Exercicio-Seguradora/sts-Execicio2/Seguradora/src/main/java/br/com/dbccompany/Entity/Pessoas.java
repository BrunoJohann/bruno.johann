package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoas {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "PESSOAS_SEQ", sequenceName = "PESSOAS_SEQ")
	@GeneratedValue(generator = "PESSOAS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	//@Column(nullable = false)
	private String nome;
	
	//@Column(unique = true, nullable = false)
	private long cpf;
	
	private String pai;
	
	private String mae;
	
	private long telefone;
	
	private String email;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "ENDERECO_PESSOAS",
			joinColumns = {
					@JoinColumn(name = "ID_PESSOAS")},
			inverseJoinColumns = {
					@JoinColumn(name = "ID_ENDERECOS")})
	private List<Endereco> enderecos = new ArrayList<Endereco>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}
	
}
