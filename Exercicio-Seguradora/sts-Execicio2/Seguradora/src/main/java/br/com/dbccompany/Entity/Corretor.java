package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "CORRETOR")
@PrimaryKeyJoinColumn(name = "ID_PESSOAS")
public class Corretor extends Pessoas{
	
	private String cargo;
	
	private double comissao;
	
	@OneToMany(mappedBy = "corretor")
	//@JoinColumn(name = "ID_SERVICOS_CONTRATADOS")
	private List<ServicoContratado> servicosContratados = new ArrayList<ServicoContratado>();

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getComissao() {
		return comissao;
	}

	public void setComissao(double comissao) {
		this.comissao = comissao;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void setServicosContratados(List<ServicoContratado> servicosContratados) {
		this.servicosContratados = servicosContratados;
	}
	
}
