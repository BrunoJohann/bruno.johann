package br.com.dbccompany.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "ID_ENDERECOS")
public class EnderecoSeguradoras extends Endereco{
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_SEGURADORA")
	private Seguradora seguradora;

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}
	
	
}
