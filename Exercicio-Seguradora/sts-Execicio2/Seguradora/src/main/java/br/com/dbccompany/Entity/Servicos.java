package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Servicos {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "SERVICOS_SEQ", sequenceName = "SERVICOS_SEQ")
	@GeneratedValue(generator = "SERVICOS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String nome;
	
	private String descricao;
	
	@JoinColumn(name = "VALOR_PADRAO")
	private long valorPadrao;
	
	@ManyToMany(mappedBy = "servicos")
	@Column(name = "ID_SEGURADORA")
	private List<Seguradora> seguradoras = new ArrayList<>();
	
	@OneToMany(mappedBy = "servicos")
	@Column(name = "ID_SERVICOS_CONTRATADOS")
	private List<ServicoContratado> servicosContratados = new ArrayList<ServicoContratado>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public long getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(long valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	public List<Seguradora> getSeguradoras() {
		return seguradoras;
	}

	public void setSeguradoras(List<Seguradora> seguradoras) {
		this.seguradoras = seguradoras;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void setServicosContratados(List<ServicoContratado> servicosContratados) {
		this.servicosContratados = servicosContratados;
	}

}
