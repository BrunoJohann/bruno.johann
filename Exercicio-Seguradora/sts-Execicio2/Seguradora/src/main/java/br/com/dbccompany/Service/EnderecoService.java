package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.EnderecoRepository;

@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository<Endereco> enderecoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco salvarEndereco(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}
	
	public Optional<Endereco> buscarEndereco(Integer id) {
		return enderecoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco editarEndereco(Integer id, Endereco endereco) {
		endereco.setId(id);
		return enderecoRepository.save(endereco);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public void deletarEndereco(Integer id) {
		enderecoRepository.deleteById(id);
	}
	
	public List<Endereco> allEnderecos() {
		return (List<Endereco>) enderecoRepository.findAll();
	}
	
	public Endereco buscarPorNumero(long numero) {
		return enderecoRepository.findByNumero(numero);
	}
	
}
