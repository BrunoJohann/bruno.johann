package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Entity.Servicos;

public interface ServicosRepository extends CrudRepository<Servicos, Integer>{

	Servicos findByNome(String nome);
	Servicos findByDescricao(String descricao);
	Servicos findByValorPadrao(long valorPadrao);
	List<Servicos> findBySeguradoras(List<Seguradora> seguradoras);
	List<Servicos> findBySeguradoras(Seguradora seguradoras);
	Servicos findByServicosContratados(List<ServicoContratado> servicosContratados);
	Servicos findByServicosContratados(ServicoContratado servicosContratados);
	
}
