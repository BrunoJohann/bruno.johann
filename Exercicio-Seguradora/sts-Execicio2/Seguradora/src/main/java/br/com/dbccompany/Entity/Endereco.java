package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Endereco {

	@Id
	@SequenceGenerator(allocationSize = 1, name = "ENDERECO_SEQ", sequenceName = "ENDERECO_SEQ")
	@GeneratedValue(generator = "ENDERECO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String logradouro;
	
	private long numero;
	
	private String complemento;
	
	@ManyToOne
	@JoinColumn(name = "ID_BAIRRO")
	private Bairro bairro;
	
	@ManyToMany(mappedBy = "enderecos")
	private List<Pessoas> pessoas = new ArrayList<Pessoas>();
	
	@OneToOne(mappedBy = "seguradora")
	private EnderecoSeguradoras enderecoSeguradora;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public List<Pessoas> getPessoas() {
		return pessoas;
	}

	public void setPessoas(List<Pessoas> pessoas) {
		this.pessoas = pessoas;
	}

	public EnderecoSeguradoras getEnderecoSeguradora() {
		return enderecoSeguradora;
	}

	public void setEnderecoSeguradora(EnderecoSeguradoras enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}
	
}
