package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTE")
public class SaldoCliente extends AbstractEntity< SaldoClienteId >{

    @EmbeddedId
    private SaldoClienteId saldoClienteId;

    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private long quantidade;

    @Column(nullable = false)
    private LocalDate vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    @Column(nullable = false)
    private List<Acessos> acessos = new ArrayList<>();

//  getSaldoClienteId
    public SaldoClienteId getId() {
        return saldoClienteId;
    }

//  setSaldoClienteId
    public void setId(SaldoClienteId saldoClienteId) {
        this.saldoClienteId = saldoClienteId;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }
}
