package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuarios, Integer> {

    Usuarios findByNome(String nome);
    Usuarios findByEmail(String email);
    Usuarios findByLogin(String login);
    Usuarios findBySenha(String senha);

}
