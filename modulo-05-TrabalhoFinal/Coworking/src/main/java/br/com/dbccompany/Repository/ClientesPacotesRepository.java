package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.ClientesPacotes;
import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Entity.Pagamentos;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes, Integer> {

    List<ClientesPacotes> findByClientes(Clientes cliente);
    List<ClientesPacotes> findByPacotes(Pacotes pacote);
    ClientesPacotes findByQuantidade(long quantidade);
    ClientesPacotes findByPagamentos(Pagamentos pagamento);

}
