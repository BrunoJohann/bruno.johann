package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contratacao extends AbstractEntity< Integer >{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "TIPO_CONTRATACAO", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private long quantidade;

    private long desconto = 0;

    @Column(nullable = false)
    private long prazo;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACOS", nullable = false)
    private Espacos espacos;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTES")
    private Clientes clientes;

    @OneToMany
    private List<Pagamentos> pagamentos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public long getDesconto() {
        return desconto;
    }

    public void setDesconto(long desconto) {
        this.desconto = desconto;
    }

    public long getPrazo() {
        return prazo;
    }

    public void setPrazo(long prazo) {
        this.prazo = prazo;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
