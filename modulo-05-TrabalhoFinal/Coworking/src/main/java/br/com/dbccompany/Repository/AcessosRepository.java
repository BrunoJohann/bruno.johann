package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface AcessosRepository extends CrudRepository<Acessos, Integer> {

    Acessos findBySaldoCliente(SaldoCliente saldoCliente);
    Acessos findByIsEntrada(long isEntrada);
    Acessos findByDate(Date date);
    Acessos findByIsExcecao(long isExcecao);

}
