package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pagamentos;
import br.com.dbccompany.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/pagamentos")
public class PagamentosController extends AbstractController< Pagamentos, Integer > {

    @Autowired
    private PagamentosService pagamentosService;

    @Override
    public List<Pagamentos> lstEntidades() {
        return pagamentosService.all();
    }

    @Override
    public Pagamentos novaEntidade( @RequestBody Pagamentos pagamento ) throws Exception {
        return pagamentosService.salvar( pagamento );
    }

    @Override
    public Optional<Pagamentos> buscar( @PathVariable Integer id ) {
        return pagamentosService.buscarPorId( id );
    }

    @Override
    public Pagamentos editarEntidade( @PathVariable Integer id, @RequestBody Pagamentos pagamento ) {
        return pagamentosService.editar( id, pagamento );
    }

}
