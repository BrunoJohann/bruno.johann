package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.EspacosPacotes;
import br.com.dbccompany.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/espacos_pacotes")
public class EspacosPacotesController extends AbstractController<EspacosPacotes, Integer >{

    @Autowired
    private EspacosPacotesService espacosPacotesService;

    @Override
    public List<EspacosPacotes> lstEntidades() {
        return espacosPacotesService.all();
    }

    @Override
    public EspacosPacotes novaEntidade( @RequestBody EspacosPacotes entidade ) throws Exception {
        return espacosPacotesService.salvar( entidade );
    }

    @Override
    public Optional<EspacosPacotes> buscar( @PathVariable Integer id ) {
        return espacosPacotesService.buscarPorId( id );
    }

    @Override
    public EspacosPacotes editarEntidade( @PathVariable Integer id, @RequestBody EspacosPacotes entidade ) {
        return espacosPacotesService.editar( id, entidade );
    }

}
