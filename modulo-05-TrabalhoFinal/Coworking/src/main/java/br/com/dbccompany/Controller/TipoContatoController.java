package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/tipo_contato")
public class TipoContatoController extends AbstractController< TipoContato, Integer > {

    @Autowired
    private TipoContatoService tipoContatoService;

    @Override
    public List<TipoContato> lstEntidades() {
        return tipoContatoService.all();
    }

    @Override
    public TipoContato novaEntidade( @RequestBody TipoContato tipoContato ) throws Exception {
        return tipoContatoService.salvar(tipoContato);
    }

    @Override
    public Optional<TipoContato> buscar( @PathVariable Integer id) {
        return tipoContatoService.buscarPorId( id );
    }

    @Override
    public TipoContato editarEntidade( @PathVariable Integer id, @RequestBody TipoContato tipoContato) {
        return tipoContatoService.editar( id, tipoContato );
    }

}
