package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Pacotes extends AbstractEntity< Integer >{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue(generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    private double valor;

    @OneToMany(mappedBy = "pacotes")
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacotes")
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
