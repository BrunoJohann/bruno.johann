package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class Acessos extends AbstractEntity< Integer >{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    private SaldoCliente saldoCliente;

    @Column(name = "IS_ENTRADA")
    private long isEntrada;

    private LocalDate date;

    @Column(name = "IS_EXCECAO")
    private long isExcecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public long getIsEntrada() {
        return isEntrada;
    }

    public void setIsEntrada(long isEntrada) {
        this.isEntrada = isEntrada;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public long getIsExcecao() {
        return isExcecao;
    }

    public void setIsExcecao(long isExcecao) {
        this.isExcecao = isExcecao;
    }
}
