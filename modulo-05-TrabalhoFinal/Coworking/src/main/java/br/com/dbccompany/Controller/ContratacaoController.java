package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Service.ContratacaoService;

@Controller
@RequestMapping("api/contratacao")
public class ContratacaoController {

	@Autowired
	private ContratacaoService contratacaoService;

	@GetMapping( value = "/" )
	@ResponseBody
	public List<Contratacao> lstEntidades() {
		return contratacaoService.all();
	}

	@PostMapping( value = "/novo" )
	@ResponseBody
	public double novaEntidade( @RequestBody Contratacao entidade ) throws Exception {
		return contratacaoService.salvarContratacao( entidade );
	}

	@GetMapping( value = "/{id}")
	@ResponseBody
	public Optional<Contratacao> buscar( @PathVariable Integer id ) {
		return contratacaoService.buscarPorId(id);
	}

	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Contratacao editarEntidade( @PathVariable Integer id, @RequestBody Contratacao entidade ) {
		return contratacaoService.editar( id, entidade );
	}
	
	
	
	
}
