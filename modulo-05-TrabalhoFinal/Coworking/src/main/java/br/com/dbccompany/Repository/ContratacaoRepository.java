package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.*;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Integer> {

    Contratacao findByTipoContratacao(TipoContratacao tipoContratacao);
    Contratacao findByQuantidade(long Quantidade);
    Contratacao findByDesconto(long desconto);
    Contratacao findByPrazo(long prazo);
    List<Contratacao> findByEspacos(Espacos espaco);
    List<Contratacao> findByClientes(Clientes cliente);
    Contratacao findByPagamentos(Pagamentos pagamento);

}
