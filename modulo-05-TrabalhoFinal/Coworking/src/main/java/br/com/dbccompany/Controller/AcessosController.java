package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Service.AcessosService;

@Controller
@RequestMapping("api/acessos")
public class AcessosController extends AbstractController< Acessos, Integer >{

	@Autowired
	private AcessosService acessosService;
	
	@Override
	public List<Acessos> lstEntidades() {
		return acessosService.all();
	}

	@Override
	public Acessos novaEntidade(  @RequestBody Acessos entidade ) throws Exception {
		return acessosService.salvar( entidade );
	}

	@Override
	public Optional<Acessos> buscar( @PathVariable Integer id ) {
		return acessosService.buscarPorId( id );
	}

	@Override
	public Acessos editarEntidade( @PathVariable Integer id, @RequestBody Acessos entidade ) {
		return acessosService.editar( id, entidade );
	}
	
	
}
