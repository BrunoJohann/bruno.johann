package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/pacotes")
public class PacotesController extends AbstractController< Pacotes, Integer > {

    @Autowired
    private PacotesService pacotesService;

    @Override
    public List<Pacotes> lstEntidades() {
        return pacotesService.all();
    }

    @Override
    public Pacotes novaEntidade( @RequestBody Pacotes entidade ) throws Exception {
        return pacotesService.salvar( entidade );
    }

    @Override
    public Optional<Pacotes> buscar( @PathVariable Integer id ) {
        return pacotesService.buscarPorId( id );
    }

    @Override
    public Pacotes editarEntidade( @PathVariable Integer id, @RequestBody Pacotes entidade ) {
        return pacotesService.editar( id, entidade );
    }

}
