package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
@Table(name = "ESPACOS_PACOTES")
public class EspacosPacotes extends AbstractEntity< Integer >{

	@Id
	@SequenceGenerator(allocationSize = 1, name = "ESPACOS_PACOTES_SEQ", sequenceName = "ESPACOS_PACOTES_SEQ")
	@GeneratedValue(generator = "ESPACOS_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;

	private long quantidade;
	
	private long prazo;

	@Column(name = "TIPO_CONTATACAO")
	@Enumerated(EnumType.STRING)
	private TipoContratacao tipoContratacao;

	@ManyToOne
	@JoinColumn(name = "ID_ESPACOS")
	private Espacos espacos;

	@ManyToOne
	@JoinColumn(name = "ID_PACOTES")
	private Pacotes pacotes;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(long quantidade) {
		this.quantidade = quantidade;
	}

	public long getPrazo() {
		return prazo;
	}

	public void setPrazo(long prazo) {
		this.prazo = prazo;
	}

	public TipoContratacao getTipoContratacao() {
		return tipoContratacao;
	}

	public void setTipoContratacao(TipoContratacao tipoContratacao) {
		this.tipoContratacao = tipoContratacao;
	}

	public Espacos getEspacos() {
		return espacos;
	}

	public void setEspacos(Espacos espacos) {
		this.espacos = espacos;
	}

	public Pacotes getPacotes() {
		return pacotes;
	}

	public void setPacotes(Pacotes pacotes) {
		this.pacotes = pacotes;
	}
}
