package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.ClientesPacotes;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Pagamentos;
import br.com.dbccompany.Entity.TipoPagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentosRepository extends CrudRepository<Pagamentos, Integer> {

    Pagamentos findByContratacao(Contratacao contratacao);
    Pagamentos findByClientesPacotes(ClientesPacotes clientesPacotes);
    List<Pagamentos> findByTipoPagamento(Enum<TipoPagamento> tipoPagamentoEnum);

}
