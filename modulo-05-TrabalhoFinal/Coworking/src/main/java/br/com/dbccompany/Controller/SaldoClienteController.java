package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoClienteId;
import br.com.dbccompany.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/saldo_cliente")
public class SaldoClienteController extends AbstractController< SaldoCliente, SaldoClienteId> {

    @Autowired
    private SaldoClienteService saldoClienteService;

    @Override
    public List<SaldoCliente> lstEntidades() {
        return saldoClienteService.all();
    }

    @Override
    public SaldoCliente novaEntidade( @RequestBody SaldoCliente saldoCliente) throws Exception {
        return saldoClienteService.salvar( saldoCliente );
    }

    @Override
    public Optional<SaldoCliente> buscar( @PathVariable SaldoClienteId id) {
        return saldoClienteService.buscarPorId( id );
    }

    @Override
    public SaldoCliente editarEntidade( @PathVariable SaldoClienteId id, @RequestBody SaldoCliente saldoCliente) {
        return saldoClienteService.editar( id, saldoCliente );
    }

}
