package br.com.dbccompany.Controller;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

public abstract class AbstractController< E, ID > {

    @GetMapping( value = "/" )
    @ResponseBody
    public abstract List< E > lstEntidades();

    @PostMapping( value = "/novo" )
    @ResponseBody
    public abstract E novaEntidade( @RequestBody E entidade ) throws Exception;

    @GetMapping( value = "/{id}")
    @ResponseBody
    public abstract Optional<E> buscar(@PathVariable ID id);

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public abstract E editarEntidade( @PathVariable ID id, @RequestBody E entidade );

}
