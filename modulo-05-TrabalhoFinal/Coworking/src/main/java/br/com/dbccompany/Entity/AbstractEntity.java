package br.com.dbccompany.Entity;

//Testar postman como interface interface
public abstract class AbstractEntity< ID > {
	
	public abstract ID getId();

    public abstract void setId( ID id );

}
