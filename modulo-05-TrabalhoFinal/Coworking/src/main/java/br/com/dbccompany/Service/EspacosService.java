package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EspacosService extends AbstractService< Espacos, Integer, EspacosRepository > {

    @Autowired
    private EspacosRepository espacosRepository;

    @Override
    public Espacos salvar(Espacos entidade) {
        return espacosRepository.save(entidade);
    }

}
