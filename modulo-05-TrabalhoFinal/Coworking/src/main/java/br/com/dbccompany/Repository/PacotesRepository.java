package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.ClientesPacotes;
import br.com.dbccompany.Entity.EspacosPacotes;
import br.com.dbccompany.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;

public interface PacotesRepository extends CrudRepository<Pacotes, Integer> {

    Pacotes findByValor(double valor);
    Pacotes findByEspacosPacotes(EspacosPacotes espacosPacotes);
    Pacotes findByClientesPacotes(ClientesPacotes clientesPacotes);

}
