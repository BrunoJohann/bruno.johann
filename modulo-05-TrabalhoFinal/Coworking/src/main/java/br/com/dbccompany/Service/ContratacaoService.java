package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.TipoContratacao;
import br.com.dbccompany.Repository.ContratacaoRepository;
import br.com.dbccompany.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ContratacaoService extends AbstractService< Contratacao, Integer, ContratacaoRepository > {

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private EspacosRepository espacosRepository;

    @Transactional(rollbackFor = Exception.class)
    public double salvarContratacao(Contratacao contratacao) throws Exception {
        double valor = espacosRepository.findById(contratacao.getEspacos().getId()).get().getValor();
        long quantidade = contratacao.getQuantidade();
        long desconto = contratacao.getDesconto();
        TipoContratacao tipoContratacao = contratacao.getTipoContratacao();
        double valorFinal;
        switch (tipoContratacao){
            case MINUTO:
                valorFinal = ((valor * quantidade)/60) - desconto; break;
            case HORA:
                valorFinal = (valor * quantidade) - desconto; break;
            case TURNO:
                valorFinal = ((valor * quantidade) * 5) - desconto; break;
            case DIARIA:
                valorFinal = ((valor * quantidade) * 8) - desconto; break;
            case SEMANA:
                valorFinal = ((valor * quantidade) * 44) - desconto; break;
            case MES:
                valorFinal = ((valor * quantidade) * 220) - desconto; break;
            default:
                valorFinal = (valor * quantidade) - desconto; break;
        }
        contratacaoRepository.save(contratacao);
        return valorFinal;
    }

    @Override
    public Optional< Contratacao > buscarPorId(Integer id) {
        return contratacaoRepository.findById(id);
    }

}
