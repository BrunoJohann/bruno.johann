package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.AbstractEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public class AbstractService< E extends AbstractEntity< ID >, ID, ERepository extends CrudRepository<E, ID> > {

    @Autowired
    private ERepository eRepository;

    @Transactional(rollbackFor = Exception.class)
    public E salvar(E entidade) throws Exception {
        return eRepository.save(entidade);
    }

    public Optional<E> buscarPorId(ID id) {
        return eRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public E editar(ID id, E entidade) {
        entidade.setId(id);
        return eRepository.save(entidade);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(ID id) {
        eRepository.deleteById(id);
    }

    public List<E> all() {
        return (List<E>) eRepository.findAll();
    }

}
