package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Service.ContatoService;

@Controller
@RequestMapping("api/contato")
public class ContatoController extends AbstractController< Contato, Integer >{

	@Autowired
	private ContatoService contatoService; 
	
	@Override
	public List<Contato> lstEntidades() {
		return contatoService.all();
	}

	@Override
	public Contato novaEntidade( @RequestBody Contato entidade ) throws Exception {
		return contatoService.salvar( entidade );
	}

	@Override
	public Optional<Contato> buscar( @PathVariable Integer id ) {
		return contatoService.buscarPorId( id );
	}

	@Override
	public Contato editarEntidade(@PathVariable Integer id, @RequestBody Contato entidade ) {
		return contatoService.editar( id, entidade );
	}
	
	
}
