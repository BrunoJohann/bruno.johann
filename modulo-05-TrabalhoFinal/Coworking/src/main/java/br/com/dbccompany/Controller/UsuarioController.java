package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Usuarios;
import br.com.dbccompany.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Usuarios> lstEntidades() {
        return usuarioService.all();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Usuarios novaEntidade(@RequestBody Usuarios usuario) throws Exception {
        return usuarioService.salvar(usuario);
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Optional<Usuarios> buscar(@PathVariable Integer id) {
        return usuarioService.buscarPorId( id );
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Usuarios editarEntidade( @PathVariable Integer id, @RequestBody Usuarios usuario ) {
        return usuarioService.editar(id, usuario);
    }

//    @GetMapping( value = "/{nome}")
//    @ResponseBody
//    public Usuarios buscar(@PathVariable String nome) {
//        return usuarioService.buscarPorNome( nome );
//    }

}
