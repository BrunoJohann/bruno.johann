package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.dbccompany.Entity.ClientesPacotes;
import br.com.dbccompany.Service.ClientesPacotesService;

@Controller
@RequestMapping("api/clientes_pacotes")
public class ClientesPacotesController extends AbstractController< ClientesPacotes, Integer >{

	@Autowired
	private ClientesPacotesService clientesPacotesService;
	
	@Override
	public List<ClientesPacotes> lstEntidades() {
		return clientesPacotesService.all();
	}

	@Override
	public ClientesPacotes novaEntidade(  @RequestBody ClientesPacotes entidade ) throws Exception {
		return clientesPacotesService.salvar( entidade );
	}

	@Override
	public Optional<ClientesPacotes> buscar( @PathVariable Integer id ) {
		return clientesPacotesService.buscarPorId( id );
	}

	@Override
	public ClientesPacotes editarEntidade( @PathVariable Integer id, @RequestBody ClientesPacotes entidade ) {
		return clientesPacotesService.editar( id, entidade );
	}
	
}
