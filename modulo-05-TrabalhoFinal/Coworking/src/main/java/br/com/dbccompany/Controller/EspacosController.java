package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("api/espacos")
public class EspacosController {

    @Autowired
    private EspacosService espacosService;

    @GetMapping( value = "/" )
    @ResponseBody
    public List<Espacos> lstEntidades() {
        return espacosService.all();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Espacos novaEntidade( @RequestBody Espacos entidade ) throws Exception{
        return espacosService.salvar( entidade );
    }

    @GetMapping( value = "/{id}")
    @ResponseBody
    public Optional<Espacos> buscar( @PathVariable Integer id ) {
        return espacosService.buscarPorId(id);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Espacos editarEntidade( @PathVariable Integer id, @RequestBody Espacos entidade) {
        return espacosService.editar( id, entidade );
    }
    
}
