package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Entity.EspacosPacotes;
import br.com.dbccompany.Entity.Pacotes;
import br.com.dbccompany.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotes, Integer> {

    EspacosPacotes findByQuantidade(long quantidade);
    EspacosPacotes findByPrazo(long prazo);
    List<EspacosPacotes> findByTipoContratacao(Enum<TipoContratacao> tipoContratacaoEnum);
    List<EspacosPacotes> findByEspacos(Espacos espacos);
    List<EspacosPacotes> findByPacotes(Pacotes pacotes);

}
