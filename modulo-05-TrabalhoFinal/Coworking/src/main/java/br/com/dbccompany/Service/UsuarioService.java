package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Usuarios;
import br.com.dbccompany.Repository.UsuarioRepository;
import br.com.dbccompany.Security.CriptografiaMD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService extends AbstractService< Usuarios, Integer, UsuarioRepository >{

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Usuarios salvar(Usuarios usuario) {
		if (usuario.getSenha().length() >= 6) {
			usuario.setSenha( new CriptografiaMD5().encode(usuario.getSenha()));
			return usuarioRepository.save(usuario);
		}
		return null;
	}

	@Override
	public Usuarios editar(Integer id, Usuarios usuario) {
		if( usuario.getSenha().length() >= 6 ) {
			usuario.setSenha( new CriptografiaMD5().encode(usuario.getSenha() ));
			usuario.setId(id);
			return usuarioRepository.save(usuario);
		}
		return null;
	}

	public Usuarios buscarPorNome(String nome) {
	    return usuarioRepository.findByNome(nome);
    }

	public Usuarios buscarPorEmail(String email) {
		return usuarioRepository.findByEmail(email);
	}

	public Usuarios buscarPorLogin(String login) {
		return usuarioRepository.findByLogin(login);
	}

}
