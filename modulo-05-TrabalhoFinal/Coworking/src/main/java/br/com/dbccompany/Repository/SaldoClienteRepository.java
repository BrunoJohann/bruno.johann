package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Entity.SaldoClienteId;
import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, SaldoClienteId> {

    List<SaldoCliente> findByTipoContratacao(Enum<TipoContratacao> tipoContratacaoEnum);
    SaldoCliente findByQuantidade(long quantidade);
    SaldoCliente findByVencimento(Date vencimento);
    SaldoCliente findByAcessos(Acessos acessos);

}
