package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Pagamentos;
import br.com.dbccompany.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentosService extends AbstractService< Pagamentos, Integer, PagamentosRepository> {

    @Autowired
    private PagamentosRepository pagamentosRepository;

    @Override
    public Pagamentos salvar(Pagamentos pagamentos) {
        boolean clientesPacotes = false, contratacao = false;
        if( pagamentos.getClientesPacotes() != null ) { clientesPacotes = true ; }
        if( pagamentos.getContratacao() != null ) { contratacao = true; }
        if( clientesPacotes || contratacao ) { return pagamentosRepository.save( pagamentos ); }
        return null;
    }

}
