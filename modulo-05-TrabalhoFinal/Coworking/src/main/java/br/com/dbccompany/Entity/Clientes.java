package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Clientes extends AbstractEntity<Integer>{

	@Id
//	@SequenceGenerator(allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
	@GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(nullable = false)
	private String nome;

	@Column(nullable = false, unique = true)
	private long cpf;

	@Column(name = "DATA_DE_NASCIMENTO", nullable = false)
	private LocalDate dataDeNascimento;

	@OneToMany(mappedBy = "clientes")
	private List<Contato> contato = new ArrayList<>();

	@OneToMany(mappedBy = "clientes")
	private List<Contratacao> contratacoes = new ArrayList<>();

	@OneToMany(mappedBy = "clientes")
	private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataDeNascimento() {
		return dataDeNascimento;
	}

	public void setDataDeNascimento(LocalDate dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

	public List<Contato> getContato() {
		return contato;
	}

	public void setContato(List<Contato> contato) {
		this.contato = contato;
	}

	public List<Contratacao> getContratacoes() {
		return contratacoes;
	}

	public void setContratacoes(List<Contratacao> contratacoes) {
		this.contratacoes = contratacoes;
	}

	public List<ClientesPacotes> getClientesPacotes() {
		return clientesPacotes;
	}

	public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
		this.clientesPacotes = clientesPacotes;
	}
}
