package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    Contato findByValor(String valor);
    List<Contato> findByTipoContato(TipoContato tipoContato);
    List<Contato> findByClientes(Clientes cliente);

}
