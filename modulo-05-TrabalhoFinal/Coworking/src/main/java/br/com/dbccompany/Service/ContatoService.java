package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContatoService extends AbstractService<Contato, Integer, ContatoRepository> {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato salvar( Contato entidade ) {
        return contatoRepository.save(entidade);
    }

    public Contato buscarPorValor( String valor ) {
        return contatoRepository.findByValor( valor );
    }

}
