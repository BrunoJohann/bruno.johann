package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espacos;
import br.com.dbccompany.Entity.EspacosPacotes;
import org.springframework.data.repository.CrudRepository;

public interface EspacosRepository extends CrudRepository<Espacos, Integer> {

    Espacos findByNome(String nome);
    Espacos findByQtdPessoas(long qtdPessoas);
    Espacos findByValor(double valor);
    Espacos findByEspacosPacotes(EspacosPacotes espacosPacotes);
    Espacos findByContratacoes(Contratacao contratacao);

}
