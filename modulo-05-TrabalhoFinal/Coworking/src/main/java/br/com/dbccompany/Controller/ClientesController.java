package br.com.dbccompany.Controller;

import java.util.List;
import java.util.Optional;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("api/clientes")
public class ClientesController {

	@Autowired
	private ClientesService clientesService;

	@GetMapping( value = "/" )
	@ResponseBody
	public List<Clientes> lstEntidades() {
		return clientesService.all();
	}

	@PostMapping( value = "/novo" )
	@ResponseBody
	public Clientes novaEntidade( @RequestBody Clientes cliente ) {
		return clientesService.salvar( cliente );
	}

	@GetMapping( value = "/{id}")
	@ResponseBody
	public Optional<Clientes> buscar( @PathVariable Integer id ) {
		return clientesService.buscarPorId( id );
	}

	@PutMapping( value = "/editar/{id}" )
	@ResponseBody
	public Clientes editarEntidade( @PathVariable Integer id, @RequestBody Clientes entidade ) {
		return clientesService.editar( id, entidade );
	}

	
	
}
