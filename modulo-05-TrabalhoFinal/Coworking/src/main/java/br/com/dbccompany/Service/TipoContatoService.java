package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends AbstractService< TipoContato, Integer, TipoContatoRepository > {

	@Autowired
	private TipoContatoRepository tipoContatoRepository;
	
	@Override
	public TipoContato salvar(TipoContato tipoContato) {
		tipoContato.setNome(tipoContato.getNome().toUpperCase());
		//tipoContato.setNome(tipoContato.getNome().substring(0,1).toUpperCase().concat(tipoContato.getNome().toLowerCase().substring(1)));
		//tipoContato.setNome(StringUtils.capitalize(tipoContato.getNome().toLowerCase()));
        return tipoContatoRepository.save(tipoContato);
    }

    public TipoContato buscarPorTipoNome( String nome ) {
		return tipoContatoRepository.findByNome( nome );
	}

}
