package br.com.dbccompany.Entity;


import javax.persistence.*;

@Entity
public class Contato extends AbstractEntity< Integer >{

	@Id
	@SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
	@GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String valor;

	@ManyToOne
	@JoinColumn(name = "ID_TIPO_CONTATO")
	private TipoContato tipoContato;

	@ManyToOne
	@JoinColumn(name = "ID_CLIENTE")
	private Clientes clientes;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public TipoContato getTipoContato() {
		return tipoContato;
	}

	public void setTipoContato(TipoContato tipoContato) {
		this.tipoContato = tipoContato;
	}

	public Clientes getClientes() {
		return clientes;
	}

	public void setClientes(Clientes clientes) {
		this.clientes = clientes;
	}
}
