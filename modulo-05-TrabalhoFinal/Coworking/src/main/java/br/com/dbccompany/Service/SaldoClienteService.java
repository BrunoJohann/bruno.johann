package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoClienteId;
import br.com.dbccompany.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaldoClienteService extends AbstractService< SaldoCliente, SaldoClienteId, SaldoClienteRepository> {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Override
    public SaldoCliente salvar(SaldoCliente entidade) {

        return saldoClienteRepository.save(entidade);
    }


}
