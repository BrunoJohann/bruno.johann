package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ClientesRepository;
import br.com.dbccompany.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientesService extends AbstractService< Clientes, Integer, ClientesRepository> {

    @Autowired
    private ClientesRepository clientesRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    @Override
    public Clientes salvar( Clientes cliente ) {
//        Para os testes
//        return clientesRepository.save( cliente );
        boolean email = false;
        boolean telefone = false;
        for (Contato contato : contatoRepository.findByClientes( cliente )) {
            if(contato.getTipoContato().getNome().equals("EMAIL")){ email = true; }
            if(contato.getTipoContato().getNome().equals("TELEFONE")){ telefone = true; }
        }
        if( email && telefone ) { return clientesRepository.save(cliente); }
        return null;
    }

    public Clientes buscarPorNome( String nome ) {
        return clientesRepository.findByNome( nome );
    }

}
