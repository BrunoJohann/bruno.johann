package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
public class Usuarios extends AbstractEntity<Integer>{

	@Id
	@GeneratedValue(generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String nome;

	@Column(unique = true)
	private String email;

	@Column(unique = true)
	private String login;
	
	private String senha;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
