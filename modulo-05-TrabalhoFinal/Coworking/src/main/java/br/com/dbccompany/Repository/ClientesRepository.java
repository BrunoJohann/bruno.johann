package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.ClientesPacotes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Contratacao;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface ClientesRepository extends CrudRepository<Clientes, Integer> {

    Clientes findByNome(String nome);
    Clientes findByCpf(long cpf);
    Clientes findByDataDeNascimento(LocalDate dataDeNascimento);
    Clientes findByContato(Contato contato);
    Clientes findByContratacoes(Contratacao contratacao);
    Clientes findByClientesPacotes(ClientesPacotes clientesPacotes);

}
