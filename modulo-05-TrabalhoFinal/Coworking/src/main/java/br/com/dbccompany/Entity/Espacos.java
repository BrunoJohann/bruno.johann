package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Espacos extends AbstractEntity< Integer >{

	@Id
	@GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
	private Integer id;

	@Column(name = "nome", unique = true)
	private String nome;

	@Column(name = "QTD_PESSOAS")
	private long qtdPessoas;

	@Column(name = "valor", nullable = false)
	private double valor;

	@OneToMany(mappedBy = "espacos")
	private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

	@OneToMany(mappedBy = "espacos")
	private List<Contratacao> contratacoes = new ArrayList<>();

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getQtdPessoas() {
		return qtdPessoas;
	}

	public void setQtdPessoas(long qtdPessoas) {
		this.qtdPessoas = qtdPessoas;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public List<EspacosPacotes> getEspacosPacotes() {
		return espacosPacotes;
	}

	public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
		this.espacosPacotes = espacosPacotes;
	}

	public List<Contratacao> getContratacoes() {
		return contratacoes;
	}

	public void setContratacoes(List<Contratacao> contratacoes) {
		this.contratacoes = contratacoes;
	}
}
