package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "CLIENTES_PACOTES")
public class ClientesPacotes extends AbstractEntity< Integer >{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_PACOTES_SEQ", sequenceName = "CLIENTES_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTES")
    private Clientes clientes;

    @ManyToOne
    @JoinColumn(name = "ID_PACOTES")
    private Pacotes pacotes;

    private long quantidade;

    @OneToMany(mappedBy = "clientesPacotes")
    private List<Pagamentos> pagamentos = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
