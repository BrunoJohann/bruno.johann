package br.com.dbccompany;

import br.com.dbccompany.Entity.Clientes;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Service.ClientesService;
import br.com.dbccompany.Service.ContatoService;
import br.com.dbccompany.Service.TipoContatoService;
import org.assertj.core.api.LocalDateAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class ClienteTest extends CoworkingApplicationTests {

    @Autowired
    private ClientesService clientesService;

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Autowired
    ApplicationContext context;

    @Test
    public void clienteSoPodeCadastrarComUmEmailEUmTelefone() {
        TipoContato tipoContatoEmail = new TipoContato();
        tipoContatoEmail.setNome("email");
        TipoContato tipoContatoTelefone = new TipoContato();
        tipoContatoTelefone.setNome("telefone");

        tipoContatoService.salvar( tipoContatoTelefone );
        tipoContatoService.salvar( tipoContatoEmail );

        Contato contatoEmail = new Contato();
        contatoEmail.setTipoContato( tipoContatoService.buscarPorTipoNome( "EMAIL" ) );
        contatoEmail.setValor("bruno@mail.com");

        Contato contatoTelefone = new Contato();
        contatoTelefone.setTipoContato( tipoContatoService.buscarPorTipoNome( "TELEFONE" ) );
        contatoTelefone.setValor("12354689");

        contatoService.salvar( contatoTelefone );
        contatoService.salvar( contatoEmail );

        Clientes cliente = new Clientes();
        cliente.setNome("Bruno");
        cliente.setCpf(123467891);
        LocalDate date = LocalDate.parse("1999-02-02");
        cliente.setDataDeNascimento(date);
        List<Contato> contatos = new ArrayList<>();
        contatos.add( contatoService.buscarPorValor( "12354689" ) );
        contatos.add( contatoService.buscarPorValor( "bruno@mail.com" ) );
        cliente.setContato( contatos );

        clientesService.salvar( cliente );

        assertThat( cliente.getContato().get(1).getTipoContato().getNome() ).isEqualTo( "EMAIL" );
        assertThat( clientesService.buscarPorNome("Bruno").getCpf() ).isEqualTo( cliente.getCpf() );

    }
}
