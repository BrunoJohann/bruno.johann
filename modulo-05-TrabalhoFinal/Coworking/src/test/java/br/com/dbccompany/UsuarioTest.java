package br.com.dbccompany;

import br.com.dbccompany.Entity.Usuarios;
import br.com.dbccompany.Security.CriptografiaMD5;
import br.com.dbccompany.Service.UsuarioService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
public class UsuarioTest extends CoworkingApplicationTests {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    ApplicationContext context;


//    @Before
//    public void setUp(){
//        Usuarios usuario = new Usuarios();
//        usuario.setNome("Marlon");
//        usuario.setEmail("teste@email");
//        usuario.setLogin("@marlon");
//        usuario.setSenha("xavrauuuuu");
//        Mockito.when(usuarioRepository.findByNome(usuario.getNome())).thenReturn(usuario);
//    }

    @Test
    public void testarSeACriptografiaTaRolando() {
        Usuarios usuario = new Usuarios();
        usuario.setNome("Bruno");
        usuario.setEmail("bruno@email");
        usuario.setLogin("brubru");
        usuario.setSenha("xavrauuuuu");

        usuarioService.salvar(usuario);

        Usuarios result = usuarioService.buscarPorNome("Bruno");
        System.out.println(result.getNome() +"/n"+ result.getSenha());
        boolean resultado = ! (result.getSenha().equals("xavrauuuuu")) ;

        assertThat(result.getSenha()).isEqualTo(new CriptografiaMD5().encode("xavrauuuuu"));
        assertThat(resultado).isEqualTo(true);
    }


}
